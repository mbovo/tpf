<?php

/*
    TPF - Tiny PHP Framework
    Welcome, this is the main entry point for everything
    You must configure your web server in order to redirect every request
    to this index.php file
    Don't worry about parameters and things like that
    TPF will take care of them for you
*/

/* Redirection workaround, point always to /xxx/index.php  */
if ( strpos( $_SERVER['REQUEST_URI'] ,"index.php") === false ){
    header( 'Location: '. dirname($_SERVER['SCRIPT_NAME']).'/index.php' );
    exit;
}

require_once('./class/TPF.php');            // Loading the main class
$TPF = new TPF();                           // creating the object
unset($TPF);                                // forcing garbage collector

?>

