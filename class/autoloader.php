<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

function cache_this_class($class_path){
    global $__AUTOLOADER_USE_CACHE;
    if ( ! file_exists('./class/.cache/') )
        assert ( mkdir( './class/.cache/' ) );
    assert ( copy ( $class_path , './class/.cache/'. basename($class_path) ) );
    echo "<!-- Caching: $class_path | $hsh -->\n";
}

/* auto load for classess caching content*/
function __autoload($class_name) {
    global $__AUTOLOADER_USE_CACHE;

    # PORK-A-ROUND avoid to die() if we are loading a template (a subclass in different directory)
    if ( strpos( $class_name, "_Template" ) !== FALSE ) return;

    // load cached class for faster loading (check md5 hash for changes)
    if ( $__AUTOLOADER_USE_CACHE ) {
        if ( file_exists( './class/.cache/' . $class_name . '.php') ){
                echo "<!-- Load from cache: ". $class_name . "-->\n";
                require_once ( './class/.cache/' . $class_name . '.php');
                return;
        }
    }

    // loop for every subdirectory "LogicalName/ and LogicalName/interfaces"
    $dirlist = dir('./class');
    if ( $dirlist && $dirlist !== false ) {
        while (false !== ($entry = $dirlist->read())) {
        if ( $entry != '..' && is_dir($dirlist->path.'/'.$entry) ){
            if ( file_exists( $dirlist->path . '/'.$entry.'/'.$class_name.'.php' ) ){
                if ( $__AUTOLOADER_USE_CACHE ) cache_this_class( $dirlist->path . '/'.$entry.'/'.$class_name.'.php' );
                require_once ( $dirlist->path . '/'.$entry.'/'.$class_name.'.php' );
                return;
            }
        }
        }
    $dirlist->close();
    }

    #ob_end_flush();
    #die("AUTOLOADER: Critical Error - Class <b>$class_name</b> not found!</em><br/>");
    return false;
}

function __classfile_exists($class_name) {

    // loop for every subdirectory "LogicalName/ and LogicalName/interfaces"
    $dirlist = dir('./class');
    if ( $dirlist && $dirlist !== false ) {
        while (false !== ($entry = $dirlist->read())) {
        if ( $entry != '..' && is_dir($dirlist->path.'/'.$entry) ){
            if ( file_exists( $dirlist->path . '/'.$entry.'/'.$class_name.'.php' ) ){
                return true;
            }
        }
        }
    $dirlist->close();
    }

    return false;
}

?>
