<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

###############################################################################
#   G L O B A L   B E H I A V O U R   V A R I A B L E S
###############################################################################

# change if you want caching classes, avoiding filesystem traversal on loading
$__AUTOLOADER_USE_CACHE=false;

###############################################################################
#   A P P L I C A T I O N   L O A D E R
###############################################################################
require_once ('./class/autoloader.php');    // code for auto-loading classes

###############################################################################
#   M A I N   C L A S S
###############################################################################
final class TPF{

    public $APP;
    private $globalconf;

    public function __construct(){
        try{
            Config::init();             // setting up the configurations-holder box
            ErrorHandler::init();       // setting up the error-handler and the Logger
            Lang::init();               // settung up languages
            Templates::init();          // setting up templates

            Datasource::init();         // setting up Datasource tools
            Auth::init();
            Plugins::init();            // calling up Plugins handler

            $this->APP = new App();     // creating a new application (FrontController)
            $this->APP->run();          // parse and serve contents

        }catch (Exception $e){
            ErrorHandler::forceEcho("Fatal Exception: ".$e->getMessage() ); // if you reach this, is really a fatal one
        }
    }

    public function __destruct(){
        unset ($this->APP);

        Plugins::destroy();
        Auth::destroy();
        Datasource::destroy();

        Templates::destroy();
        Lang::destroy();
        Config::destroy();
        ErrorHandler::destroy();
    }

    public function loadGlobals(){
        $this->globalconf = parse_ini_file("./data/conf/global.ini",true);
    }
}

###############################################################################
#   S A N I T Y  C H E C K S
###############################################################################
if (!defined('PHP_VERSION_ID')) {
    $version = explode('.', PHP_VERSION);

    define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
}
if (intval(PHP_VERSION_ID) < 50400){
    die('CRITICAL: TPF require PHP 5.4 or above, seems you are running the incompatible '.PHP_VERSION." ".PHP_VERSION_ID);
}


###############################################################################
#   S T A T I C   F U N C T I O N S
###############################################################################
    /*
    public static function sanitizeStr($str){
        //TODO sanitize this string
        force_echo ('<!-- TODO: sanitizeStr(), it does nothing for now -->');
        return $str;
    }

*/

?>
