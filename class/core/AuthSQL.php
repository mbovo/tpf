<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

final class AuthSQL{

    private static $config;

    public final static function init(){
        self::$config = Config::get(__CLASS__);
        Config::addClass(__CLASS__,' loaded '.count(self::$config));
      }


      public final static function login($uname=null,$upwd=null){

        $db = Datasource::loadAndConnect(self::$config['db_dsn']);
        if ( !$db ){
            Logger::log( __METHOD__ . " | Datasource returns an empty object " , ERR);
            return false;
        }

        $uname=$db->quote($uname);

        $qry = "SELECT ";

        $first=true;
        foreach ( self::$config['db_get_field'] as $field){
            if (!$first)
                $qry.=', ';
            else
                $first=false;
            $qry .= $field;
        }

        $realpwd="";
        switch (self::$config['db_auth_method']){
            case 'md5':
                $realpwd=$db->quote(md5($upwd));
                break;
            default:
                $realpwd=$db->quote($upwd);
        }

        $qry .= " FROM ".self::$config['db_table'] . " WHERE ".self::$config['db_auth_user']." = $uname AND ".self::$config['db_auth_password']." = $realpwd ";

        if ( isset(self::$config['db_auth_optional']) && self::$config['db_auth_optional'] != "" )
            $qry .= ' AND '.self::$config['db_auth_optional'];

        $qry.=";";

        Logger::log(__METHOD__ . " | $qry ",DEBUG);
        $pdo = $db->query($qry);
        if ( $pdo===false){
            Logger::log( __METHOD__ . " | error on DB->query() ".$db->errorInfo()[2] , ERR);
            return false;
        }

        $v = $pdo->fetchAll();
        $results=array();
        if (count($v) > 0 ){
            foreach ( self::$config['db_get_field'] as $field){
                $results[$field] = $v[0][$field];
            }
            return $results;
        }
        return false;
    }

}
