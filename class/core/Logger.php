<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
/**
 * Logger generic class
 *
 */


    const EMERG=LOG_EMERG;
    const ALERT=LOG_ALERT;
    const CRIT=LOG_CRIT;
    const ERR=LOG_ERR;
    const WARN=LOG_WARNING;
    const NOTICE=LOG_NOTICE;
    const INFO=LOG_INFO;
    const DEBUG=LOG_DEBUG;
    const DISABLED=-1;

class Logger {
    /* error mapping PHP Errors -> logger error */
    private static $errormap = array (
                                        E_ERROR              => 'Error',
                                        E_WARNING            => WARN,
                                        E_PARSE              => ALERT,
                                        E_NOTICE             => NOTICE,
                                        E_CORE_ERROR         => EMERG,
                                        E_CORE_WARNING       => ALERT,
                                        E_COMPILE_ERROR      => CRIT,
                                        E_COMPILE_WARNING    => WARN,
                                        E_USER_ERROR         => ERR,
                                        E_USER_WARNING       => WARN,
                                        E_USER_NOTICE        => INFO,
                                        E_STRICT             => DEBUG,
                                        E_RECOVERABLE_ERROR  => WARN
                                    );
    /* ini file human values to error constants */
    public static $errorval = array(
                                    "off"       => DISABLED,
                                    "disabled"  => DISABLED,
                                    "debug"     => LOG_DEBUG,
                                    "info"      => LOG_INFO,
                                    "notice"    => LOG_NOTICE,
                                    "warning"   => LOG_WARNING,
                                    "error"     => LOG_ERR,
                                    "critical"  => LOG_CRIT,
                                    "alert"     => LOG_ALERT,
                                    "emergency" => LOG_EMERG,
                                );

    private static $enabled = true;
    private static $level = -1;
    private static $facility = null;

    public static function init(){
        $conf = Config::get('Logger');

        if ( $conf['enable'] == '0' ){
            self::$enabled = false;
            return false;
        }

        self::$level = self::$errorval[ strtolower( $conf['level'] ) ];

        $facilityname = $conf['facility'];

        if ( __classfile_exists($facilityname) ){
            self::$facility = new $facilityname(self::$level, self::$enabled );
            if (!self::$facility){
                ErrorHandler::forceEcho("<!--FATAL: ".__METHOD__."()  the requested facility [".$facilityname."] not found -->");
                self::$enabled = false;
                return false;
            }
       } else{
            ErrorHandler::forceEcho("<!--FATAL: ".__METHOD__."()  the requested facility [".$facilityname."] not found -->");
            self::$enabled = false;
            return false;
        }
        Config::addClass(__CLASS__,' facility='.$facilityname);
    }

    public static function destroy(){
        if ( !self::$enabled ) return false;
       self::$facility->__destruct();
    }

    public static function log($value,$prio=NOTICE) {
        if ( !self::$enabled ) return false;
        return self::$facility->log($value,$prio);
    }

    public static function setLevel($value){
        if ( !self::$enabled ) return false;
        return self::$facility->setLevel($value);
    }

    public static function disable(){
        if ( !self::$enabled ) return false;
        return self::$facility->disable();
    }

    public static function enable(){
        return self::$facility->enable();
    }

    /* exec a generic method $m to the hinstance of logger*/
    public static function exec(){
        // todo
        return false;
    }


    public static function map($val){
        if ( array_key_exists($val, self::$errormap) )
            return self::$errormap[$val];
        else
            return DISABLED; // are you jokin? ok lets me answer the same joke :)
    }

}

?>
