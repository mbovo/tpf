<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
/*
 * Plugin Manager, handle Modules aka Plugins, loads, startup and methods
 *  a module must be a class inside a file, inside a subdirectory of "inc/plugins" with the same name
 *  and should extends class "Plugin"
 */
class Plugins {

    private static $map=array();
    private static $config=array();
    private static $errors=array();

    public final static function init(){
        // get configuration
        self::$config = Config::get('Plugins');
        $enabled = Config::get('Plugins','enabled');

        if ( isset($enabled) && is_array($enabled ) ){
            foreach ( $enabled as $modname ){
                self::forceLoad($modname);
            }
        }
        Config::internals('Plugins::loaded',self::$map);
        Config::internals('Plugins::errros',self::$errors);
        Config::addClass(__CLASS__,'loaded '.count(self::$map).' plugins');
    }

    public static function destroy(){
        self::$map = NULL;
        Logger::log(__METHOD__ . " | unloaded.",DEBUG);
    }


    public static function childs(){
        return self::$map;
    }

    // return the object
    public static function get($name){
        if ( isset(self::$map[$name]) )
            return self::$map[$name];
        Logger::log(__METHOD__ . " | Plugin [$name] not loaded.",WARN);
        return null;
    }

    public static function exists($name){
        if (self::get($name)) return true;
        return false;
    }

    // force loading of a plugin, despite of the configuration in App.ini
    public static final function forceLoad($modname){

        // check if file exists
        $path = self::$config['root'] . '/'.  $modname . '/'. $modname . '.php';
        $errors = array();
        if ( file_exists( $path ) ){

            // try to import the file
            try{
                include_once($path);
            }catch(Exception $e) {
                    Logger::log(__METHOD__ . " | Error loading [".$modname."] plugin | ".$e->getMessage(),ERR);
                    return;
            }

            // check if it's a subclass of Plugin:
            try{
                if ( !is_subclass_of($modname, 'Plugin') )
                    throw new Exception('Not a subclass of "Plugin"');
            }catch(Exception $e){
                    Logger::log(__METHOD__ . " | Error loading [".$modname."] plugin | ".$e->getMessage(),ERR);
                    return;
            }

            // Get module configuration (ie: static class or not)
            if ( Config::get('Plugin.'.$modname, 'static') == '1' ){
                // is a static class, try to call module::init()
                try{
                    $method= array($modname,'init');
                    if ( is_callable($method) )
                        forward_static_call_array($method, array($modname, Config::get('Plugin.'.$modname, 'init_args') ) );
                    else
                        throw new Exception('public static function init() not found in class <b>'.$modname.'</b>!');
                    self::$map[$modname] = new $modname();      // create the object only for names
                }catch(Exception $e) {
                    Logger::log(__METHOD__ . " | Error calling [".$modname."] init() method | ".$e->getMessage(),ERR);
                    return;
                }
                #Config::set('loaded_plugins',self::$map);
            } else {
                // Load the dynamic object:
                try{
                    self::$map[$modname] = new $modname();
                }catch(Exception $e){
                    Logger::log(__METHOD__ . " | Error calling [".$modname."()] | ".$e->getMessage(),ERR);
                    return;
                }

            }

        }else
            Logger::log(__METHOD__ . " | Failed loading [".$modname."] plugin | No such file or directory in $path",ERR);

    }


    /* Exec a function call to specified plugin, it works with both static classes and dynamic objects */
    public static final function exec($modname=null,$funxname='',$args=array()){
        if ( !$modname ) return null;
        if ( !$funxname) return null;

        if ( self::child($modname)=='' ) {
            Logger::log(__METHOD__ . " | Plugin [".$modname."] not loaded",WARN);
            return null;
        }

        $method = array( $modname, $funxname );
        if ( !is_callable($method) ) return null;

        if ( Config::get('Plugin.'.$modname, 'static')=='1' ){
            return forward_static_call_array($method, $args );
        }else{
             return call_user_func_array( $method, $args );
        }
    }

}

class Plugin{

    protected $p_name;
    protected $p_version;

    // Please override this method
    public function __construct(){  }

    // Please override this method
    public function getname(){ return $this->p_name; }

    // Please override this method
    public function getversion(){ return $this->p_version; }

    // Please override this methods, returns true on success false otherwise
    public function run(){ return false; }

}

?>
