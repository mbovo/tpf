<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

class Config {

    private static $conf=array();               // current loaded configuration from .ini files (SAVED)
    private static $iconf=array();              // current internal configurationr  (NOT SAVED)
    public static $debug=false;             // shortcut to Config::get('global','debug') if setted

    public static function init(){

        eval(base64_decode('CiAgICAgICAgc2VsZjo6aW50ZXJuYWxzKCAnZWxwdGltZScsIG1pY3JvdGltZSh0cnVlKSApOwogICAgICAgIHNlbGY6OmludGVybmFscyggJ25hbWUnLCAiVFBGIC0gVGlueSBQZXJzb25hbCBGcmFtZXdvcmsiICk7CiAgICAgICAgc2VsZjo6aW50ZXJuYWxzKCAndmVyJywgIjMuMCIgKTsKICAgICAgICBzZWxmOjppbnRlcm5hbHMoICdjb2RlbmFtZScsICJyZWRfcGFuZGEiKTsKICAgICAgICBzZWxmOjppbnRlcm5hbHMoICdhdXRob3InLCAiTWFudWVsIEJvdm8iICk7CiAgICAgICAgc2VsZjo6aW50ZXJuYWxzKCAnbG9hZGVkJywgYXJyYXkoKSApOwo='));
        // SETTING INTERNALS
        self::$iconf['basepath'] = realpath('./');
        self::$iconf['class_loaded'] = array(__CLASS__ => 'OK');

        // LOAD GLOBAL CONFIG FILE
        self::loadConfFile('global');

        if ( self::$conf['global']['debug'] == '1' ) self::$debug=true;

        // INITIAL SETUP
        date_default_timezone_set( self::get('global','default_timezone') );
        self::ini_set_handler();
        session_set_cookie_params( self::get('global','session_timeout') );
        session_start();
        self::$iconf['sid'] = session_id();
        self::restoreSession();

        // LOAD OTHER CONFIG FILE AS STATED IN global.ini
        if (is_array (self::$conf['Config']['include']) )
            foreach ( self::$conf['Config']['include'] as $file )
                self::loadConfFile($file);
        else
            self::loadConfFile( self::$conf['Config']['include'] );

        // SAVING SERVER VARIABLES
        self::$iconf['SERVER'] = $_SERVER;

    }

    public static function destroy(){

        //TODO: base64encoding
        $elptime = self::internals('elptime');
        $epltime = microtime(true) - $elptime;
        ErrorHandler::forceEcho ('<!-- Generated in | '.$epltime.' secs. | '.self::internals('name').' | '.self::internals('codename') . ' | ver. ' .self::internals('ver') . ' | by ' . self::internals('author') . ' -->');
        if ( self::$debug ){
            ErrorHandler::forceEcho ('<!-- ######################### Yo Bro - SpUT iT OuT #################################-->');
            self::showConf();
            self::showInternals();
            self::showFunctions();
            ErrorHandler::forceEcho ('<!-- ################################################################################-->');
        }
        self::saveSession();
    }

    public static function get($sect="global",$key=null,$subkey=null){

        if ( isset (self::$conf[$sect]) ){
            if ( $key==null && $subkey==null )
                return self::$conf[$sect];          //return the sections
            elseif ( $key!=null && $subkey==null )
                if ( isset( self::$conf[$sect][$key] ))
                    return self::$conf[$sect][$key];    //return the key
            elseif ( $key!=null && $subkey!= null )
                if ( isset( self::$conf[$sect][$key]) && is_array( self::$conf[$sect][$key] ) )
                    return self::$conf[$sect][$key][$subkey];   //return a subkey
        }
        return null;
    }


    public static function set($val=null,$sect="global",$key=null,$subkey=null){
        if ( !$val ) return false;
        if ( !$key && !$subkey ){
            self::$conf[$sect] = $val;
            return true;
        }elseif( $key && !$subkey ){
            self::$conf[$sect][$key] = $val;
            return true;
        }elseif ( $key && $subkey && is_array( self::$conf[$sect][$key] ) ){
            self::$conf[$sect][$key][$subkey] = $val;
            return true;
        }
        return false;
    }

    public static function del($sect=null,$key=null,$subkey=null){
        if ( ! $sect ) return false;
        if ( ! $key  && ! $subkey )
            unset(self::$conf[$sect] );
        elseif ( $key && !$subkey )
            unset(self::$conf[$sect][$key]);
        elseif ( $key && $subkey )
            unset(self::$cont[$sect][$key][$subkey]);
        return true;
    }

    public static function internals($key=null,$val=null){
        if ( !$key ) return null;
        if ( !$val ){   // no value? return the current value if any
            if ( isset( self::$iconf[$key] ) )
                return self::$iconf[$key];
            else
                return null;
        }else{
            self::$iconf[$key] = $val;
            return self::$iconf[$key];
        }
    }

    public static function getAll(){
        return self::$conf;
    }

    public static function getAllInternals(){
        return self::$iconf;
    }

    public static function loadConfFile($name){
        $ext = "";
        if ( strpos($name, ".php" ) === false )
            $ext = ".php";
        $path = self::$iconf['basepath'] . '/data/conf/' . $name . $ext;
        if ( is_readable($path ) ){
            $res = parse_ini_file($path,true);
            if (is_array($res) ) {
                self::$conf = array_merge(self::$conf,$res);
                self::$iconf['conf_loaded'][] = $path;
                return true;
            }
        }
        return false;
    }

    public static function saveSession(){
    #   if ( self::$conf['config']['persistant']=='1')
    #       foreach ( self::$conf as $k => $v )
    #           $_SESSION[$k] = $v;
    }

    public static function setSession($k=null,$v=null){
        if (!$k) return false;
        if (!$v) return false;
        $_SESSION[$k] = $v;
        return true;
    }

    public static function restoreSession(){
        foreach ($_SESSION as $k => $v )
            self::set($v,"session",$k);
    }

    public static function addClass($classname,$status="OK"){
        $loaded = self::$iconf['class_loaded'];
        if ( !is_array($loaded) )
            $loaded = array();

        if ( array_key_exists($classname, $loaded ) ) return false;
        $loaded[$classname] = $status;
        self::$iconf['class_loaded'] = $loaded;
        return true;
    }

    private static function ini_set_handler(){
        // call ini_set for each value in global.ini
        $iniconfig = self::$conf['phpini'];
        $inifailure = array();
        foreach ( $iniconfig as $name => $value ){
            if ( ini_set($name, $value) === false )
                $inifailure[$name] = $value;
        }
        self::$iconf['iniset_failures'] = $inifailure;
    }

    ### DEBUG purpose ###

    public static function showConf(){
        ErrorHandler::forceEcho("<!-- " . print_r(self::$conf,true) . " -->" );
    }

    public static function showInternals(){
        ErrorHandler::forceEcho("<!-- " . print_r(self::$iconf,true) . " -->" );
    }

    public static function showFunctions(){
        $functions = get_defined_functions();
        $functions_list = array();
        foreach ($functions['user'] as $func) {
            $f = new ReflectionFunction($func);
            $args = array();
            foreach ($f->getParameters() as $param) {
                $tmparg = '';
                if ($param->isPassedByReference()) $tmparg = '&';
                if ($param->isOptional()) {
                        $tmparg = '[' . $tmparg . '$' . $param->getName() . ' = ' . $param->getDefaultValue() . ']';
                } else {
                        $tmparg.= '&' . $param->getName();
                }
                $args[] = $tmparg;
                unset ($tmparg);
            }
                $functions_list[] =  $func . ' ( ' . implode(', ', $args) . ' )' ;
        }
        ErrorHandler::forceEcho ("<!-- ". print_r($functions_list,true) ." -->");
    }


}

?>
