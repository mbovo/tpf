<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software; 
 * you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation; 
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

/*
 *
 * Generic Template, you must extend this class to implement a working template
 *
 * 	You can insert specific configurations in any config files loaded like that:
 *	'TemplateName' must be the name of template (case insensitive) defined as $p_name in your class
 *
 *	Config::set('template.TemplateName::configname','configvalue');	
 * 	Config::set('template.TemplateName::configarray', array('arg1','arg2', ... , 'argn') ); 
 *
 */
class Template{

	// external propreties, you MUST declared these on your child class
	protected $p_name=" Template() class ";
	protected $p_version="?.?";
	protected $p_copy="If you see this line your template is misconfigured.";


	// internal status
	protected $p_display_destroy=true;	// display() on destroy() ?
	protected $is_out=false;			// already output data? 
	protected $footer_added = false;	// in order to avoid double footing
	protected $header_added = false;	// in order to avoid double heading
	protected $buffer=array();			// the buffer
	protected $config=array();			// local configuration from Config
	protected $strip=false;				// strip outpu

	public function __construct($display_on_destroy=true){
		$this->config = Config::get('Template.'.$this->p_name);
		$this->strip = Config::get('Templates','strip_output') == '1' ? true : false;
		$this->putHeader();
		$this->p_display_destroy = $display_on_destroy;
		Config::set( $this->getTemplateCopyLine(), "Templates","copyline" );
	}
	
	public function __destruct(){ 
		if ( $this->p_display_destroy )
			$this->display();
	}

	public function putHeader(){
		if ( ! $this->header_added ) { 
			$path = Config::get('Templates','root');
			$path .= '/' . $this->p_name . '/tpl/header.html'; 

			if ( ! is_readable( $path ) || ! $this->loadTplFile($path) ){
					Logger::log(__METHOD__ . " file $path not found, fallback",WARN);
					$this->out ('<!DOCTYPE html><html><head><body>');
			}
					
			$this->header_added = true;
		}
		// a warning
		#echo "<font color='red'>Seems you have disabled Silent Output. Please add <b>silence_output = 1<b> in <em>[ErrorHandler]</em> section of <b>global.ini</b></font>" ;
	}
	
	public function putFooter(){ 
		if ( ! $this->footer_added ){
			$path = Config::get('Templates','root');
			$path .= '/' . $this->p_name  . '/tpl/footer.html'; 

				if ( ! is_readable( $path ) || ! $this->loadTplFile($path) ){
					Logger::log(__METHOD__ . " file $path not found, fallback",WARN);
						$this->out ('</body></html>');
				}
				
				$this->footer_added = true;
		}
	}
	
	public function getTemplateCopyLine(){
		if ($this->getVersion() != "" && $this->getCopyright()!="" )
			return "Template [" . $this->getName() . "] | v." . $this->getVersion() . " | " . $this->getCopyright() ;
		elseif ( $this->getCopyright() != "" )
			return "Template [" . $this->getName() . "] " . $this->getCopyright() ;
		else
			return "Template [" . $this->getName() ."]" ;
	}
	
	public function out($html){
		$this->buffer[] = $html;
	}
	
	public function putFile($file){
		$path = Config::get('Templates','root');
		$path .= '/' . $this->p_name . '/tpl/' . str_replace( array('/','..') ,'',$file); 
			if ( ! is_readable( $path ) || ! $this->loadTplFile($path) )
				Logger::log(__METHOD__ . " file $path not found.",WARN);
	}
	
	// load and parse a .tpl file
	protected function loadTplFile($path){

		$tpl_file = file_get_contents($path);
		if ($tpl_file===false) return false;
		
		//parsing functions
		$res = $this->parseToken( $tpl_file,"{{" ,"}}");
		if ( $res !== false ){
			foreach ($res as $k=>$r){
				$args = $this->parseToken( $r , "(", ")");
				$resargs=array();
				$p=strtok( $args[0], ",");
				while( $p !== false ){
					$resargs[] = $p;
					$p = strtok(",");
				}
				$res[$k] = array ( substr( $r, 0,  strpos( $r, "(")  ) , $resargs );
			}
			$tpl_file = $this->applyCallback( $tpl_file, $res );		
		}
		
		// parsing variables
		$res = $this->parseToken( $tpl_file,"%%" ,"%%");
		if ( $res!==false )
			$tpl_file = $this->substituteVars( $tpl_file, $res );
		
#		if $this->config[]
		
		$this->out( $tpl_file );
		return true;
	}
	
	public function substituteVars( $text, $vars ){
		$out = $text;
		if ( is_array($vars) )
			foreach ( $vars as $id => $var){
				$parent= strtok($var,".");
				$child = strtok(".");
				$subchild = strtok(".");
				$out = str_replace( '%%' . $id. '%%', Config::get($parent,$child,$subchild), $out );
			}
		if ( $out && $out!="" ) return $out;
		return $text;
	}
	
	public function applyCallback ( $text, $callbacks ){
		$out = $text;
		if ( is_array($callbacks) )
			foreach ( $callbacks as $id => $func ){
				// you must use only function alias defined somewhere you cannot call class!
				$class = strtok($func[0],".");
				$meth = strtok(".");
				if ( function_exists( $class ) || (class_exists($class) && method_exists( $class, $meth) ) ){
#DEBUG					force_echo("<!-- applyCallback() ".$func[0]." exists! -->\n");
#DEBUG					force_echo("\t<!-- applyCallback() calling ". $func[0] . '(' . $func[1][0] . ',' . $func[1][1] . ')' . "-->\n");
					$args = $func[1];
					#$res_str = $func[0] ( $args[0], $args[1], $args[2] );
					if ( $meth!="" )
						$res_str = call_user_func_array( array( $class, $meth ), $args );
					else
						$res_str = call_user_func_array( $class, $args );

					if ( is_string( $res_str ) )
						#$out = str_replace( '{{' . $func[0] .'('. $args[0] .','. $args[1] .')' . '}}',  $res_str , $out );
						$out = str_replace( '{{' . $id . '}}',  $res_str , $out );
					else
						Logger::log(__METHOD__." |  ".$func[0]." return something (not a string) ". $res_str ,ERR);
				}else
					Logger::log(__METHOD__. " function [$class $meth] unknown",ERR);
			}
		if ($out && $out!="") return $out;
		return $text;
	}
	
	// is not multiline !
	protected function parseToken( &$str,$token_start=NULL, $token_stop=NULL){
		if ( ! $token_start || ! $token_stop) return false;
		
		$occur = array();
		$inner = false;
		$id = 0;
		$doc = explode("\n", $str);
		
		foreach ( $doc as $line ){	
			$garbage = strtok($line,$token_start);
			if ( $garbage === false ) continue;
	
			$data = strtok($token_stop);
			if ( $data !== false && $data != ""){
				$occur[] = str_replace( str_split($token_start . $token_stop ) , "", $data) ;
				$str = str_replace(  str_replace( str_split($token_start . $token_stop ) , "", $data) , $id++, $str);
			}
		}
		if ( count($occur) < 1 ) return false;	
		return $occur;
	}
	
	// Freeing the buffer 
	public function free(){
		if ( !$this->is_out && Config::get('debug')==1 )
			ErrorHandler::forceEcho ('<!-- WARNING: Template->free() | There was content in buffer not yet written!-->');
		if ( !$this->is_out ) Logger::log(__METHOD__ . " | Trashing unwritten data! ", WARN);
		unset( $this->buffer );
		$this->buffer=array();
	}
	
	// output the content of the buffer
	public function display($force=false){
		#ErrorHandler::restoreOutput();
		
		if ($this->strip)
			$this->buffer = str_replace(array("\t","\r\n", "\n", "\r"),"",$this->buffer);
			
		if ( $force || !$this->is_out ){
			$this->putFooter();	
			foreach ( $this->buffer as $line )
				ErrorHandler::forceEcho ($line ); //."\n");
				
			$this->is_out = true;
		}
	}
	
	public function getName(){ return $this->p_name; }
	
	public function getVersion(){ return $this->p_version; }
	
	public function getCopyright(){ return $this->p_copy; }

	public function getPath($c,$n,$e) { return Templates::getPath($c,$n,$e); }
}

?>
