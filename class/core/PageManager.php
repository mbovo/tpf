<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
/*
 * Main Manager, handle pages,
 */

class PageManager implements ManagerController{

    private static $errors=array();
    private static $pages=array();
    private static $config=array();
    private static $avoid_loop = 0;

    public static function g(array $params){
        return self::get($params);
    }

    public static function get(array $params){
        if ( empty($params) || empty($params[0]) || $params[0]=="" )
            return self::home();
        return self::page($params);
    }

    public static function page(array $params){

        $p=$params[0];
        #if ( $bwlcheck ){
            if ( self::canAccess($p) == false ){
                Logger::log( __METHOD__ . " | page [".$p."] not authorized, check your black/white list configuration", WARN );
                Config::set('e401','PageManager','eIMG');
                Config::set('notauthorized','PageManager','eTITLE');
                Config::set('notauthorizedtext','PageManager','eTEXT');
                self::error();
                return;#throw new RuntimeException("403: Not authorized.");
            }
        #}

        $path = @self::$pages[ $p ];
        if ( isset( $path ) && $path!='' && is_readable ($path) )
            include_once( $path );
        else{
            Logger::log( __METHOD__ . " | page [$p] not found.", ERR );
            if ( self::$avoid_loop++ > 2 )
                throw new RuntimeException("Loop detected: Both Error page and requested page not found. Check your configuration.");
            self::error();
        }
        return true;
    }


    public static function init(){

        self::$config = Config::get('PageManager');

        // search for valid *.php in page root directory

        $rootdir = dir(self::$config['root'] );
        if ( !$rootdir || $rootdir === false){
            Logger::log( __METHOD__ . " | : Unable to read page root directory [".self::$config['root'].']' , CRIT);
            throw new Exception("Cannot open page root directory. Check your config file.");
        }

        while (false !== ($entry = $rootdir->read() )){
            $path = $rootdir->path . '/' . $entry;
            if ( $entry != '..' && !is_dir($path) ){
                $pathinfo = pathinfo($path);
                if ( is_readable($path) && $pathinfo['extension'] == 'php' )
                    self::$pages[ basename($entry,".php") ] = $path;
                else
                    self::$errors[] = "File $entry is not readable or invalid";
            }
        }
        $rootdir->close();

        //

        if ( Config::$debug ){
            Config::internals('PagesManager::pages',self::$pages);
            Config::internals('PagesManager::errors',self::$errors);
        }
        Config::set('null','PageManager','eIMG');
        Config::addClass(__CLASS__,'Loaded '.count(self::$pages).' pages');
    }

    public static function destroy(){
        //goobye world!
    }

    protected static function canAccess($p){
        // exception for index and error default pages
        if ( $p == self::$config['homepage'] || $p == self::$config['errorpage'] ) return true;

        if ( strtolower( self::$config['use_list'] ) == 'white')
            if ( in_array($p, self::$config['list'] ) )
                return true;
            else{
                Logger::log(__METHOD__ . ' | Whitelist enabled, but page ['.$p.'] not listed.',NOTICE);
                return false;
            }
        elseif ( strtolower( self::$config['use_list'] ) == "black" )
            if ( in_array($p, self::$config['list'] ) ){
                Logger::log(__METHOD__ . ' | Blacklist enabled, and page ['.$p.'] is listed.',NOTICE);
                return false;
            }else
                return true;
        // b/w list disabled, go on
        Logger::log(__METHOD__ . ' | B/W lists disabled, serving page ['.$p.'].',INFO);
        return true;
    }

    public static function home(){
        return self::page( array( self::$config['homepage'] ), false );

    }

    public static function error($req=null){
        if ( Config::get('PageManager','eIMG') == 'null' )      Config::set('e404','PageManager','eIMG');
        if ( !Config::get('PageManager','eTEXT')  ) Config::set('requestpagenotfound','PageManager','eTEXT');
        if ( !Config::get('PageManager','eTITLE')  ) Config::set('notfound','PageManager','eTITLE');
        return self::page( array( self::$config['errorpage']) , false );
    }

    public static function getURL($pagename,$module="page"){
        if ( strtolower($pagename) == 'home')
            $str = Config::get('app','webroot');
        elseif ( strtolower($pagename) == 'error')
            $str = Config::get('app','webroot') .'/'.$module.'/error';
        else
            $str = Config::get('app','webroot') . '/'.$module.'/' . $pagename;

        return $str;
    }

}

/* a common trick to call init() when this file si parsed by __autoload() function*/
PageManager::init();

?>
