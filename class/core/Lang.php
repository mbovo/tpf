<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
/**
 * Config class keep global variables between all object and functions.
 *      read values from GET, POST or COOKIES is demanded to getvar()
 */

class Lang{

    public static $map;
    public static $lang;

    public static function get($word){
        Logger::log( __METHOD__ . " Requested $word -> map -> ".self::$map[$word]."] ",DEBUG);
        if ( isset( self::$map[$word]) )
            return self::$map[$word];
        else return "_".$word."_";
    }

    public static function getClientPreferredLang(){
        $int = Config::internals('SERVER');
        if ( !isset( $int["HTTP_ACCEPT_LANGUAGE"] ) ) {
            Logger::log( __METHOD__ . " HTTP_ACCEPT_LANGUAGE seems not set. ",INFO);
            return NULL;
        }
        $languages = strtolower( $int["HTTP_ACCEPT_LANGUAGE"] );
        $languages = str_replace( ' ', '', $languages );
        $languages = explode( ",", $languages );
        $full=array();
        $primary=array();
        foreach ( $languages as $language_list ){
                // pull out the language, place languages into array of full and primary string structure:
                $temp_array = array();
                // slice out the part before ; on first step, the part before - on second, place into array
                $full[] = substr( $language_list, 0, strcspn( $language_list, ';' ) );//full language
                $primary[] = substr( $language_list, 0, 2 );// cut out primary language
        }
        // $full contains a list of preferred languages (like en-us )
        // $primary contains a list of preffered languages, only the first part (like en for en-us)
        #return $primary[0];
        return $primary;
    }

    public static function init(){
        $conf = Config::get('Lang');

        $langlist = null;

        if ( $conf['ask_browser'] == '1' )
            $langlist = self::getClientPreferredLang();         // read browser preferred languages in order

        if (!$langlist)
            $langlist = array( $conf['default'] );

        foreach ($langlist as $lang ){
            $path = './data/lang/' . $lang . ".lang";
            if ( is_readable($path) ){
                Logger::log( __METHOD__ . " | loading [$lang] from file $path ",DEBUG);
                self::$map = parse_ini_file($path,false);
                break;
            }else
                Logger::log( __METHOD__ . " | [$lang] language but file $path is nonexistant or not readable.",WARN);
        }
        Config::addClass(__CLASS__,'OK ( '.count(self::$map).' definitions)');
        #if ( !Config::import('lang.'.$lang) ) Config::import('lang.en');   /* try to load it or fall to english */
        #Config::set('translation',self::$translation);
    }

    public static function destroy(){
        self::$map = NULL;
        Logger::log(__METHOD__ . " | goodbye.",DEBUG);
    }

}

//function alias
function lang($key){
    return Lang::get($key);
}


?>
