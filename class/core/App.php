<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */


// This is a True Front Controller
final class App implements FrontController{

    private $stop=false;
    protected $manager="TestManager";
    protected $action="get";
    protected $params=array("home");

    // you can call it passing an array("manager" => "YourManager", "action"=>"yourmethod", "params"=>array("arg1","argN") )
    //
    public function __construct(array $opt = array() ){

        if ( Config::get('app','default_manager') != "" )
            $this->manager=Config::get('app','default_manager');
        if ( Config::get('app','default_method') != "" )
            $this->action=Config::get('app','default_method');

        if ( Config::get('app','default_params') != "" )
            $this->params=Config::get('app','default_params');

        try{
            if ( empty($opt))
                $this->parseURI();
            else{
                if ( isset($opt["manager"]) )
                    $this->setManager($opt["manager"]);
                if ( isset($opt["action"]) )
                    $this->setAction($opt["action"]);
                if ( isset($opt["params"]) )
                    $this->setParams($opt["params"]);
            }
            Config::addClass(__CLASS__," Loaded ".$this->manager."->".$this->action."(".str_replace ("\n","", print_r($this->params,true)).")");

        }catch (Exception $e){
            self::errorReporter($e->getMessage());
            $this->stop=true;
        }
    }

    public function setManager($manager){
        $manager = str_replace("manager","",strtolower($manager) ); //removing "Manager" occurency
        $manager = ucfirst(strtolower($manager)) . "Manager";
        if ( !class_exists($manager) ){
        # we don't use class_exists to avoid calling __autoloader so we can trap the error
#       $cl = get_declared_classes();
#       if ( !in_array($manager,$cl)){
            Logger::log(__METHOD__ . " | Manager [$manager] not defined",CRIT);
            $this->stop=true;
            header('HTTP/1.1 400 Bad Request');
            header('Status: 400 Bad Request');
            throw new RuntimeException("Cannot find Handler [".str_replace("Manager","",$manager)."], Bad Request");
        }
        $this->manager = $manager;
        return $this;
    }

    public function setAction($action){
        $ref = new ReflectionClass($this->manager);
        if ( ! $ref->hasMethod($action) ){
            if ( !$ref->hasMethod("get") ){
                Logger::log(__METHOD__ . " | Action [$action] not defined for manager [".$this->manager."]",CRIT);
                $this->stop=true;
                header('HTTP/1.1 400 Bad Request');
                header('Status: 400 Bad Request');
                throw new RuntimeException("Cannot find method [$action], Bad Request");
            }
            $this->action = "get";
        }else
            $this->action = $action;
        return $this;
    }

    public function setParams(array $params){
        $this->params = $params;
        return $this;
    }

    public function run(){
        if ( ! $this->stop )
            try{
                if ( false === call_user_func_array( array( $this->manager, $this->action), array( $this->params ) ) )
                    throw new Exception("Generic error getting the specified path");
            }catch (Exception $e){
                self::errorReporter($e->getMessage());
            }
    }

    public function __destruct(){
        // close db connection and clean up
    }

//CHANGE: do not put directly an exception, present the error page
    public static final function errorReporter($reason=null){

        if ( Config::get('app','mask_exception') ){

            header('HTTP/1.0 501 Internal Server Error');
            $cf = Config::get('PageManager');
            Config::set("contactadmin",'PageManager','eTEXT');
            Config::set("internalerror",'PageManager','eTITLE');

            Config::internals("App::errors", $reason);

            return PageManager::page( array( $cf['errorpage']) , false );

        }else{
            $error_msg = Config::get('app','error_msg');
            $stringa = "";
            foreach ( $error_msg as $msg )
                $stringa .= $msg;
            if ( ! $reason ) $reason = "General error" ;
            $stringa = str_replace("%reason%",$reason,$stringa);
            $stringa = str_replace("%title%",Config::get('app','title'),$stringa);

            ErrorHandler::forceEcho( $stringa );
            $t=Templates::get();

            $t->free();
        }
    }


    protected function parseURI(){
        $path = $_SERVER["REQUEST_URI"];

        Logger::log("App::parseURI -> ".$path,DEBUG);

        $path = str_replace( Config::get('app','webroot'), '', $path );         // removing webroot
        $path = str_replace( 'index.php', '', $path );                          // removing index.php
        $path = trim($path, "/" );                      // stripping slash
        $path = preg_replace('/[^a-zA-Z0-9]\//',"",$path);

        $plist = @explode("/",$path,3);
        $manager = isset($plist[0]) ? $plist[0] : "";
        $action = isset($plist[1]) ? $plist[1] : "";
        $params = isset($plist[2]) ? $plist[2] : "";

        //@list($manager,$action,$params) = @explode("/",$path,3);

        $params = explode ("/",$params);

        if ( isset($manager) && $manager != "" )
            $this->setManager($manager);
        if ( isset($action) && $action != "" )
            $this->setAction($action);
        if ( isset($params) && is_array($params) )
            $this->setParams($params);

    }

}

?>
