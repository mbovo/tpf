<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
/**
 * Config class keep global variables between all object and functions.
 *      read values from GET, POST or COOKIES is demanded to getvar()
 */

class ErrorHandler{

    private static $silence=false;
    private static $stub = false;
    private static $dump = false;
    private static $dumpdir = "";
    private static $errortype = array (
                                        E_ERROR              => 'Error',
                                        E_WARNING            => 'Warning',
                                        E_PARSE              => 'Parsing Error',
                                        E_NOTICE             => 'Notice',
                                        E_CORE_ERROR         => 'Core Error',
                                        E_CORE_WARNING       => 'Core Warning',
                                        E_COMPILE_ERROR      => 'Compile Error',
                                        E_COMPILE_WARNING    => 'Compile Warning',
                                        E_USER_ERROR         => 'User Error',
                                        E_USER_WARNING       => 'User Warning',
                                        E_USER_NOTICE        => 'User Notice',
                                        E_STRICT             => 'Runtime Notice',
                                        E_RECOVERABLE_ERROR  => 'Catchable Fatal Error'
                                    );


    public static function init(){
            $conf = Config::getAll();
            // all static classes should add this line
            Config::addClass(__CLASS__);
            $clog = $conf['Logger'];
            if ($conf['ErrorHandler']['silence_output']=='1'){
                self::$silence = true;
                self::stubOutput();
            }

            if ( $conf['ErrorHandler']['dump'] ) self::$dump=true;
            self::$dumpdir = $conf['ErrorHandler']['dumpdir'];

            if ( $conf['ErrorHandler']['silence_errors']=='1'){
                error_reporting(0);
            }
            set_error_handler("ErrorHandler::__err_handler", E_ALL | E_STRICT | E_DEPRECATED | E_CORE_ERROR | E_CORE_WARNING  );

            Logger::init();

    }

    public static function destroy(){

        if ( self::$dump && self::$dumpdir != "" ){
            self::dump();
        }

        Logger::destroy();
        self::restoreOutput();
        restore_error_handler();
        error_reporting(-1);
    }


    public static function __err_handler($errno, $errstr, $errfile=null, $errline=null, $errcontext=null){

        #if ( ! self::$silence )
         #   self::forceEcho( ":> ".self::$errortype[$errno]." | $errstr on $errfile @ $errline | ".print_r($errcontext,true) );
        #else
            Logger::log(" $errstr on $errfile @ line $errline | ".print_r($errcontext,true), Logger::map($errno) );

        return false;
    }

    private static function dump(){
        if ( !is_writeable(self::$dumpdir) ) return;
        $path = self::$dumpdir . "/" . Config::internals('sid') . '_' . date( Config::get('ErrorHandler','dump_file_format') ) . '.dump';
        if ( !touch($path) ) return;

        Logger::log( __METHOD__ . "  Writing dump file for sid ".Config::internals('sid'),INFO);
        file_put_contents($path,print_r( Config::getAll(),true) );
        file_put_contents($path,print_r( Config::getAllInternals(),true) ,FILE_APPEND);
        return;
    }

    public static function stubOutput(){
        if ( ! self::$silence ) return true;
        if ( ! self::$stub ){
            ob_start ( "ErrorHandler::__stub_echo_output_" );
            self::$stub = true;
            return true;
        }
        return false;
    }

    public static function restoreOutput(){
        if ( ! self::$silence ) return true;
        if ( self::$stub ){
            ob_end_flush();
            self::$stub = false;
            return true;
        }
        return false;
    }

    public static function flushOutput(){
        if ( ! self::$silence ) return true;
        return ob_flush();
    }

    public static function forceEcho($str){
        ErrorHandler::restoreOutput(); echo $str; echo "\n"; ErrorHandler::stubOutput();
    }

    // force cleaning the buffer, is called automat. by ob_end_flush() and co..
    public static function __stub_echo_output_($buf){ return ""; }

}


?>
