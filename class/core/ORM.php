<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
/*
 * Implementation of a generic ORM, your object must extend this class
 * in order to use this facility
 */

class ORM{

    protected $__PRIMARY_KEY = 'id';
    protected $__LOADED = false;

    public function PK($pk=null){
        if ($pk)
            $this->__PRIMARY_KEY = $pk;

        return $this->__PRIMARY_KEY;
    }

    public function delete(){
        $r = new ReflectionClass($this);

        $tablename = strtolower($r->getShortName()) . "s";

        $prop = $r->getProperty($this->__PRIMARY_KEY);

        $qry = "DELETE FROM $tablename WHERE ".$this->__PRIMARY_KEY ." = " . "'". $prop->getValue($this) ."';" ;

        Logger::log( __METHOD__ . " | object [".$r->getShortName()."] key [". $prop->getValue($this) ."]" , DEBUG);

        try{
            $db = Datasource::get(); // last connection
            $res = $db->query($qry);
            if ($res === false){
                Logger::log( __METHOD__ . " | query() error: ".print_r($db->errorInfo(),true) , ERR);
                return false;
            }
        }catch (Exception $e){
            Logger::log( __METHOD__ . " | delete() exception: ".$e->getMessage() , ERR);
            return false;
        }
        return true;
    }

    protected function __save__insert(){
        $r = new ReflectionClass($this);

        $tablename = strtolower($r->getShortName()) . "s";

        $qry = "INSERT INTO $tablename (";

        $values=array();

        $props = $r->getProperties(ReflectionProperty::IS_PUBLIC);
        $first=true;
        foreach( $props as $prop ){
            $p = $prop->getValue($this);
            if ( !$p || $p=="" || !isset($p) || empty($p) ) continue;

            if (!$first)
                $qry .= ", ";
            else
                $first=false;

            $qry .= $prop->name;
            $values[] = "'".$prop->getValue($this)."'";
        }

        $first=true;
        $qry .= ") VALUES (";
        foreach ($values as $v)
            if (!$first)
                $qry .= ", ".$v ;
            else{
                $qry .= $v ;
                $first=false;
            }
        $qry.= ");";
        return $qry;
    }

    protected function __save__update(){
        $r = new ReflectionClass($this);

        $tablename = strtolower($r->getShortName()) . "s";

        $qry = "UPDATE $tablename SET ";

        $values=array();

        $props = $r->getProperties(ReflectionProperty::IS_PUBLIC);
        $first=true;
        foreach( $props as $prop ){
            $p = $prop->getValue($this);
            if ( !$p || $p=="" || !isset($p) || empty($p) ) continue;

            if (!$first)
                $qry .= ", ";
            else
                $first=false;

            $qry .= $prop->name . " = '".$prop->getValue($this)."'";
        }

        $prop = $r->getProperty($this->__PRIMARY_KEY);

        $qry.= " WHERE ".$this->__PRIMARY_KEY . " = '" . $prop->getValue($this) . "' LIMIT 1;" ;
        return $qry;
    }

    public function save(){

        $r = new ReflectionClass($this);
        $prop = $r->getProperty($this->__PRIMARY_KEY);

        if ($this->__LOADED)
            $qry = $this->__save__update();
        else
            $qry = $this->__save__insert();

        Logger::log( __METHOD__ . " | object [".$r->getShortName()."] key [". $prop->getValue($this) ."]" , DEBUG);
        try{
            $db = Datasource::get(); // last connection
            $res = $db->query($qry);
            if ($res === false){
                Logger::log( __METHOD__ . " | query() error: ".print_r($db->errorInfo(),true) , ERR);
                return false;
            }
        }catch (Exception $e){
            Logger::log( __METHOD__ . " | save() exception: ".$e->getMessage() , ERR);
            return false;
        }
        return true;
    }

    public function debug(){
        return print_r($this,true);
    }


    public function load($oper=null,$pk=null){

        if (!$pk) return false;
        if (!$oper) $oper='=';

        $r = new ReflectionClass($this);
        $tablename = strtolower($r->getShortName()) . "s";

        $qry= "SELECT * FROM $tablename WHERE ".$this->__PRIMARY_KEY . " $oper '$pk';";

        Logger::log( __METHOD__ . " | object [".$r->getShortName()."] key [". $pk ."]" , DEBUG);
        $db = Datasource::get();
        if ( $db ){
            $pdo = $db->query($qry);
            if ( $pdo===false){
                Logger::log( __METHOD__ . " | query() error: ".print_r($db->errorInfo(),true) , ERR);
                throw new Exception("query() error: ".print_r($db->errorInfo(),true) );
            }

            $res = $pdo->fetchAll (PDO::FETCH_CLASS, $r->getShortName() );
            $obj = $res[0];

            if ( !$obj )
                Logger::log( __METHOD__ . " | Query returns empty set  [$qry]",DEBUG);
        }else{
            Logger::log( __METHOD__ . " | Empty database object " , ERR);
            throw new Exception("Empty database object");
        }

        try{
            $props = $r->getProperties(ReflectionProperty::IS_PUBLIC);
            foreach($props as $prop){
                $name = $prop->getName();
                if (isset($obj->$name) )
                    $prop->setValue($this, $obj->$name);
            }
        }catch (ReflectionException $e){
            Logger::log( __METHOD__ . " | Error loading object from database ".$e->getMessage , ERR);
        }

        $this->__LOADED = true;

        unset($pdo);
        unset($res);
        unset($ojb);

        return true;

    }

    /* return an array with all primary key values, in order to iterate and construct objects*/
    public static function enumerate($class){

        $obj = new $class;
        //TODO: check if is subclass of ORM here or die()
        $PKEY = $obj->PK();

        $db = Datasource::get(); // last connection
        $tablename = strtolower($class) . "s";

        $res = $db->query("SELECT $PKEY FROM $tablename");
        $v = $res->fetchAll(PDO::FETCH_ASSOC);

        $result = array();
        foreach ( $v as $val )
            $result[] = $val[$PKEY];

        return $result;
    }

}

?>
