<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
/*
 * Static handler for many DB objects
 */

class Datasource {

    private static $errors=array();
    private static $pool=array();
    private static $lastid=null;
    private static $enabled=false;

    public static function init(){
        // load configurations
        $conf = Config::get('Datasource');
        if (!$conf){
            self::$errors[] = "Unable to find Datasource configuration. (Have you loaded datasource.ini?)";
            Logger::log( __METHOD__ . " | : Unable to find Datasource configuration. (Have you loaded datasource.ini?)" , ERR);
        }else{
            self::$enabled = $conf['enabled'];

            if (!self::$enabled){
                Logger::log(__METHOD__ . " | not enabled",NOTICE);
                return null;
            }

            if($conf['auto_connect']==true){
                $ok = self::loadAndConnect( $conf['auto_connect_dbname'],$conf['auto_connect_class']);
                if (!$ok && $conf['auto_connect_required'] ){
                    self::$errors[] = "DB Connection failed and connection is required (check datasource.ini)";
                    Logger::log( __METHOD__ . " | : DB connection failed and connection is required (check datasource.ini)" , CRIT);
                    throw new Exception("DB Connection failed and connection is required (check datasource.ini)");
                }
            }
        }
        if ( Config::$debug ){
            Config::internals('DBEngine::errors',self::$errors);
            Config::internals('DBEngine::pool',self::$pool);
            Config::internals('DBEngine::conf',$conf);
        }
        Config::addClass(__CLASS__,' OK ( '.count(self::$pool)." connection in pool)");
    }

    public static function destroy(){
        self::closeAll();
        Logger::log(__METHOD__ . " | goodbye.",DEBUG);
    }

/*
 *  Retrieve configuration from config file (default datasource.ini but can be anything)
 *  and connect to database called "$dbname"
 *  returns null or connection ID
 */
    public static function loadAndConnect($dbname,$class="DB"){

        if (!self::$enabled){
            Logger::log(__METHOD__ . " | ".__CLASS__." is currently disabled ",NOTICE);
            return null;
        }

        Logger::log( __METHOD__ . " | dbname: $dbname - class $class " , DEBUG);
        $dbconf = Config::get('db.'.$dbname);
        if ( !$dbconf ){
            Logger::log( __METHOD__ . " | Configuration not found for ".$dbname."" , WARN);
            return null;
        }

        $localconf = array();
        $localconf['type']=( isset($dbconf['type'])) ? $dbconf['type'] : null;
        $localconf['host']=( isset($dbconf['host'])) ? $dbconf['host'] : null;
        $localconf['port']=( isset($dbconf['port'])) ? $dbconf['port'] : null;
        $localconf['name']=( isset($dbconf['name'])) ? $dbconf['name'] : null;
        $localconf['user']=( isset($dbconf['user'])) ? $dbconf['user'] : null;
        $localconf['pass']=( isset($dbconf['pass'])) ? $dbconf['pass'] : null;
        $localconf['persistent']=( isset($dbconf['persistent'])) ? $dbconf['persistent'] : null;

/*tooo verbose -_-

         foreach($localconf as $k=>$lc )
            if (!$lc)
                Logger::log( __METHOD__ . " |                                [$k] not set!" , DEBUG);
            else
                if ( $k=='pass' )
                    Logger::log( __METHOD__ . " |                                [$k] = ***** " , DEBUG);
                else
                    Logger::log( __METHOD__ . " |                                [$k] = $lc" , DEBUG);
*/
        $dsn = DB::_buildConnString($localconf['type'],$localconf['host'],$localconf['port'],$localconf['name']).';user='.$localconf['user'];
        Logger::log( __METHOD__ . " | Loaded configuration for [$dbname] DSN: $dsn" , INFO);

        try{
            $oldconn = self::searchByName($dsn);
            if ( !$oldconn )
                $r = new $class($localconf);
            else{
                $r = $oldconn;
                Logger::log( __METHOD__ . " | Found previous CID with same DSN [".$r->getID()."]" , NOTICE);
            }

            if ($r){
                self::$lastid = $r->getID();
                self::$pool[self::$lastid] = $r;
                return $r;
            }
        }catch (Exception $e){
            Logger::log( __METHOD__ . " | Error creating DB object: [".$e->getMessage()."]" , ERR);
        }
        return null;
    }

/*
 *  Close a specified PDO object (connection)
 */
    public static function close($id=null){

        if (!self::$enabled){
            Logger::log(__METHOD__ . " | ".__CLASS__." is currently disabled ",NOTICE);
            return null;
        }

        if (!$id) $id=self::$lastid;
        if (isset(self::$pool[$id])){
            try{
                self::$pool[$id]->__destruct();
                unset(self::$pool[$id]);
                Logger::log( __METHOD__ . " | Connection closed $id" , INFO);
                return true;
            }catch (Exception $e){
                Logger::log( __METHOD__ . " | Error while closing [".$e->getMessage()."]" , ERR);
                return false;
            }
        }else
            Logger::log( __METHOD__ . " | Trying to close an invalid CID: $id" , WARN);
        return false;
    }
/*
 *  Close all PDO connections and remove all objects
 */
    public static function closeAll(){
        Logger::log( __METHOD__ . " | Closing all connections" , DEBUG);
        try {
            foreach (self::$pool as $id => $h ){
                Logger::log( __METHOD__ . " | Destroying ".$id , DEBUG);
                self::$pool[$id]->__destruct();
                unset(self::$pool[$id]);
            }
            return true;
        } catch (Exception $e){
            Logger::log( __METHOD__ . " | Error while closing connection [".$e->getMessage()."]" , ERR);
            return false;
        }
    }

    public static function get($id=null){

        if (!self::$enabled){
            Logger::log(__METHOD__ . " | ".__CLASS__." is currently disabled ",NOTICE);
            return null;
        }

        if (!$id) $id=self::$lastid;
        try{
            if ( isset(self::$pool[$id]))
                return self::$pool[$id];
            else
                throw new Exception ("Unknown CID [$id]");
        }catch (Exception $e){
            Logger::log( __METHOD__ . " | get() failed: ".$e->getMessage() , ERR);
        }
        return null;
    }


    public static function searchByName($dsn){

        if (!self::$enabled){
            Logger::log(__METHOD__ . " | ".__CLASS__." is currently disabled ",NOTICE);
            return null;
        }

        Logger::log( __METHOD__ . " | Search for previous opened connection with DSN: $dsn" , DEBUG);
        try{
            foreach (self::$pool as $db){
                if ($db->dsn == $dsn )
                    return $db;
            }
        }catch (Exception $e){
            Logger::log( __METHOD__ . " | searchByName() failed: ".$e->getMessage() , ERR);
        }
        return null;
    }
}

/*
 * old functional class (not tested)
 class Datasource {

    private static $errors=array();
    private static $pool=array();
    private static $lastid=null;

    public static function init(){
        // load configurations
        $conf = Config::get('DBEngine');
        if (!$conf){
            self::$errors[] = "Unable to find DBEngine configuration. (Have you loaded datasource.ini?)";
            Logger::log( __METHOD__ . " | : Unable to find DBEngine configuration. (Have you loaded datasource.ini?)" , ERR);
        }else{
            if($conf['auto_connect']==true){
                $ok = self::loadAndConnect( $conf['auto_connect_dbname'] );
                if (!$ok && $conf['auto_connect_required'] ){
                    self::$errors[] = "DB Connection failed and connection is required (check datasource.ini)";
                    Logger::log( __METHOD__ . " | : DB connection failed and connection is required (check datasource.ini)" , CRIT);
                    throw new Exception("DB Connection failed and connection is required (check datasource.ini)");
                }
            }
        }
        if ( Config::$debug ){
            Config::internals('DBEngine::errors',self::$errors);
            Config::internals('DBEngine::pool',self::$pool);
            Config::internals('DBEngine::conf',$conf);
        }
        Config::addClass(__CLASS__,'Loaded.');
    }

    public static function destroy(){
        self::closeAll();
        Logger::log(__METHOD__ . " | goodbye.",DEBUG);
    }

/
    public static function loadAndConnect($dbname){
        $dbconf = Config::get('db.'.$dbname);
        if ( !$dbconf ){
            Logger::log( __METHOD__ . " | Configuration not found for ".$dbname.")" , WARN);
            return null;
        }

        $localconf = array();
        $localconf['']
        $localconf['type']=( isset($dbconf['type'])) ? $dbconf['type'] : null;
        $localconf['host']=( isset($dbconf['host'])) ? $dbconf['host'] : null;
        $localconf['port']=( isset($dbconf['port'])) ? $dbconf['port'] : null;
        $localconf['name']=( isset($dbconf['name'])) ? $dbconf['name'] : null;
        $localconf['user']=( isset($dbconf['user'])) ? $dbconf['user'] : null;
        $localconf['pass']=( isset($dbconf['pass'])) ? $dbconf['pass'] : null;
        $localconf['persistent']=( isset($dbconf['persistent'])) ? $dbconf['persistent'] : null;

        Logger::log( __METHOD__ . " | Loaded configuration [$dbname] " , DEBUG);
        foreach($localconf as $k=>$lc )
            if (!$lc)
                Logger::log( __METHOD__ . " |                                [$k] not set!" , DEBUG);
            else
                Logger::log( __METHOD__ . " |                                [$k] not set!" , DEBUG);

        $s=self::_buildConnString( $type, $host, $port, $name );
        $r=self::connect($s, $dbconf['user'], $dbconf['pass'], $persist );
        if ($r)
            self::$schema[$r] = new Schema( $dbname, $s );
        return $r;
    }


    public static function connect($conn_string,$dbuser=NULL,$dbpass=NULL,$persistent=false){
        if ( !isset($conn_string)) {
            Logger::log( __METHOD__ . " | : Missing argument: conn_string" , ERR);
            return null;
        }
        try {
            Logger::log( __METHOD__ . " | Trying PDO connection: [$dbuser:".md5($dbpass)."] @ [$conn_string] " , INFO);
            if ($persistent)
                $dbh= new PDO($conn_string,$dbuser,$dbpass,array( PDO::ATTR_PERSISTENT => true) );
            else
                $dbh= new PDO($conn_string,$dbuser,$dbpass);

            #self::$lastid = rand( getrandmax() / 2, getrandmax()) + rand(1,10);
            self::$lastid = self::getCID($conn_string, $dbuser, $dbpass);
            self::$pool[self::$lastid]=$dbh;
            Logger::log( __METHOD__ . " | Connected! CID= ".self::$lastid , DEBUG);
            return self::$lastid;
        } catch (PDOException $e){
            Logger::log( __METHOD__ . " | PDO Exception: ".$e->getMessage().")" , ERR);
            return null;
        }
    }


    public static function getCID($conn_string,$user,$pass){
        return md5("c=". $conn_string . ";u=" . $user . ";p=" .$pass);
    }


    public static function close($id=null){
        if (!$id) $id=self::$lastid;
        if (isset(self::$pool[$id])){
            self::$pool[$id]=NULL;
            unset(self::$pool[$id]);
            Logger::log( __METHOD__ . " | Connection closed $id" , INFO);
            return true;
        }else
            Logger::log( __METHOD__ . " | Trying to close an invalid CID: $id" , WARN);
        return false;
    }

    public static function closeAll(){
        Logger::log( __METHOD__ . " | Closing all connections" , INFO);
        $str="";
        try {
            foreach (self::$pool as $k => $h ){
                $str .= "\n\t\tClosing CID: $h";
                self::$pool[$k]=NULL;
                unset(self::$pool[$k]);
            }
            Logger::log( __METHOD__ . " | $str" , DEBUG);
            return true;
        } catch (Exception $e){
            Logger::log( __METHOD__ . " | Error closing connection [".$e->getMessage()."]" , ERR);
            return false;
        }
    }


    private static function _buildConnString($dbtype='mysql',$dbhost='localhost',$dbport=NULL,$dbname=NULL) {
        $conn_string=NULL;
        if ( isset($dbtype) ) $conn_string = $dbtype.':';
        if ( isset($dbhost) ) $conn_string .= 'host='. $dbhost .';' ;
        if ( isset($dbport) ) $conn_string .= 'port='. $dbport .';' ;
        if ( isset($dbname) ) $conn_string .= 'dbname='. $dbname .'' ;
        return $conn_string;
    }


    public static function escapeStr($data,$id=null){
        if (!$id) $id=self::$lastid;
        try{
            if ( !get_magic_quotes_gpc() )
                $res = strip_tags( addslashes($data) ,Config::get('global','allow_html_tags'));
            else
                $res = strip_tags( $data ,Config::get('global','allow_html_tags'));

            $res = self::$pool[$id]->quote($res);

        }catch (Exception $e){
            Logger::log( __METHOD__ . " | Escape String failed! ".$e->getMessage() , ERR);
            $res="";
        }
        return $res;
    }


}


?>
*/

?>
