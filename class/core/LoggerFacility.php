<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
/**
 * The basic logger class, extend this to implement your logger
 *
 */

class LoggerFacility{

    /* CONSTANT to Human readable error values for PHP error */
    protected $humansPHPerror = array (
                                    E_ERROR              => 'Error',
                                    E_WARNING            => 'Warning',
                                    E_PARSE              => 'Parsing Error',
                                    E_NOTICE             => 'Notice',
                                    E_CORE_ERROR         => 'Core Error',
                                    E_CORE_WARNING       => 'Core Warning',
                                    E_COMPILE_ERROR      => 'Compile Error',
                                    E_COMPILE_WARNING    => 'Compile Warning',
                                    E_USER_ERROR         => 'User Error',
                                    E_USER_WARNING       => 'User Warning',
                                    E_USER_NOTICE        => 'User Notice',
                                    E_STRICT             => 'Runtime Notice',
                                    E_RECOVERABLE_ERROR  => 'Catchable Fatal Error'
                                );
    /* CONSTANT to Human readable error values for internal logger  */
    protected $humanserror = array(
                                    DISABLED    => "DISABLED ",
                                    DEBUG       => "DEBUG    ",
                                    INFO        => "INFO     ",
                                    NOTICE      => "NOTICE   ",
                                    WARN        => "WARNING  ",
                                    ERR         => "ERROR    ",
                                    CRIT        => "CRITICAL ",
                                    ALERT       => "!ALERT!  ",
                                    EMERG       => "EMERGENCY",
                                );
    protected $level;
    protected $lastlevel;
    protected $configured_level;
    protected $enabled = true;

    public function __construct($level,$enabled=true){
        $this->level = $level;
        $this->lastlevel = $level;
        $this->configured_level = $level;
        $this->enabled = $enabled;
    }

    public function log($value,$prio){           // log a message with priority $prio
        // nothing to do here
        return true;
    }

    public function setLevel($value){
        if ( !$this->enabled ) return;
        if ( $value<=DEBUG && $value>=EMERG ) {
            $this->level = $value;
            $this->log(__METHOD__ . ' - Log level is now '. $this->humanserror[$value] ,INFO);
        }
    }

    public function disable(){
        if ( !$this->enabled ) return;
        if ( $this->level != DISABLED )
            $this->lastlevel = $this->level;
        $this->setLevel(DISABLED);
    }

    public function enable(){
        if ( !$this->enabled ) return;
        if ( $this->lastlevel != DISABLE )
            $this->setLevel($this->lastlevel);
        else
            $this->setLevel( $this->configured_leve );
    }
}

?>
