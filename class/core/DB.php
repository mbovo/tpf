<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
/*
 * This is basic ORM class, represent a standard DB
 * handle connection to a specific hinstance of DB type (mysql,pgsql,....)
 */

class DB{

        protected $h = null;
        protected $id = null;
        protected $conn_str = null;
        protected $cred = array();

        public $dsn=null;

        // arguments:
        // 1    array(type,host,port,dbname,user,pass,persistent)
        // 2    $conn_string, array($user,$pass)
        // 3    $conn_string, array($user,$pass), $persistent
        // 7    $type, $host, $port, $dbname, $user, $pass, $persistent
        public function __construct(){
                $conn_string = null;
                $cred = null;
                $persistent = null;
                switch (func_num_args()){
                                case 1:
                                        $a=func_get_arg(0);
                                        if (!is_array($a) || count($a)!=7)
                                                throw new Exception("Unabled to create: Argument 1 must be an array(type,host,port,dbname,user,pass,persistent) ");

                                        $s = array();
                                        foreach ($a as $v)
                                            $s[] = $v;

                                        $conn_string = self::_buildConnString( $s[0],$s[1],$s[2],$s[3]);
                                        $cred = array($s[4],$s[5]);
                                        $persistent = $s[6];
                                        break;
                                case 2:
                                        $conn_string = func_get_arg(0);
                                        $cred = func_get_arg(1);
                                        if (!is_array($cred) || count($cred) != 2 )
                                                throw new Exception("Unable to create: Argument 2 must be an array(user,password) ");
                                        $persistent = false;
                                        break;
                                case 3:
                                        $conn_string = func_get_arg(0);
                                        $cred = func_get_arg(1);
                                        if (!is_array($cred) || count($cred) != 2 )
                                                throw new Exception("Unable to create: Argument 2 must be an array(user,password) ");
                                        $persistent = func_get_arg(2);
                                        break;
                                case 7:
                                        $conn_string = self::_buildConnString(func_get_arg(0),func_get_arg(1),func_get_arg(2),func_get_arg(3));
                                        $cred = array(func_get_arg(4),func_get_arg(5));
                                        $persistent = func_get_arg(6);
                                        break;
                                default:
                                        throw new Exception("Unable to create: wrong number of parameters");
                                        return;
                }
                $this->dsn = $conn_string.";user=".$cred[0];
                $this->cred = $cred;
                $this->conn_str = $conn_string;
                if ( !$this->_connect($conn_string,$cred[0],$cred[1],$persistent) )
                        throw new Exception("Unable to create: connect() fails");


        }

        public function __destruct(){
                unset($this->h);
                unset($this->id);
                unset($this->conn_str);
                unset($this->cred);
                return true;
        }

        public function __toString(){
                $str ="";
                $str .= "DB object [".$this->id."]";
                if ($this->h)
                        $str .= " connected to (".$this->conn_str.") @ ".$this->cred[0]."  ".md5($this->cred[1])."\n";
                return $str;
        }

/***********************************************************************/
/***********************************************************************/
/***********************************************************************/


    private function _connect($conn_string,$dbuser=NULL,$dbpass=NULL,$persistent=false){
        try {
            Logger::log( __METHOD__ . " | Trying PDO connection: [$dbuser:".md5( "password" /*$dbpass*/)."] @ [$conn_string] " , INFO);
            if ($persistent)
                $dbh= new PDO($conn_string,$dbuser,$dbpass,array( PDO::ATTR_PERSISTENT => true) );
            else
                $dbh= new PDO($conn_string,$dbuser,$dbpass);

            $this->id = self::_makeCID($conn_string, $dbuser, $dbpass);
            Logger::log( __METHOD__ . " | Connected! CID= ".$this->id ." DNS [".$this->dsn."]", DEBUG);
            $this->h = $dbh;
            return true;
        } catch (PDOException $e){
            Logger::log( __METHOD__ . " | PDO Exception: ".$e->getMessage().")" , ERR);
            return null;
        }
    }

    public static function _buildConnString($dbtype='mysql',$dbhost='localhost',$dbport=NULL,$dbname=NULL) {
        $conn_string=NULL;
        if ( isset($dbtype) ) $conn_string = $dbtype.':';
        if ( isset($dbhost) ) $conn_string .= 'host='. $dbhost .';' ;
        if ( isset($dbport) ) $conn_string .= 'port='. $dbport .';' ;
        if ( isset($dbname) ) $conn_string .= 'dbname='. $dbname .'' ;
        return $conn_string;
    }

    private static function _makeCID($conn_string,$user,$pass){
        return md5("c=". $conn_string . ";u=" . $user . ";p=" .$pass);
    }


/***********************************************************************/
/***********************************************************************/
/***********************************************************************/



    public function getID(){
        return $this->id;
    }


    public function query($qry){
        //check here
            return $this->h->query($qry);

        return NULL;
    }

    public function exec($qry){
        //check here
                return $this->h->exec($qry);

        return NULL;
    }

    public function rowCount($pdo){
                if ($pdo) $pdo->rowCount();
                return null;
    }

    public function errorCode(){
        return $this->h->errorCode();
    }

    public function errorInfo(){
        return $this->h->errorInfo();
    }

/*
 *  Uses various functions to escape special characters basing on current connection
 */
    public function quote($data){
        try{
            if ( !get_magic_quotes_gpc() )
                $res = strip_tags( addslashes($data) ,Config::get('global','allow_html_tags'));
            else
                $res = strip_tags( $data ,Config::get('global','allow_html_tags'));

            $res = $this->h->quote($res);

        }catch (Exception $e){
            Logger::log( __METHOD__ . " | Escape String failed! ".$e->getMessage() , ERR);
            $res="";
        }
        return $res;
    }

}

?>
