<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
/*
 * DB Connection handler, open/close db connections and manage queries
 */


class Schema {
    private  $schema=array();
    private  $dbname="";
    private  $connstring="";

    public function __construct($dbname, $connstring ){
            $this->connstring=$connstring;
        $this->dbname = $dbname;
        $tables = Config::get('db.'.$dbname,'tables');
        $n=0;
        foreach ($tables as $table){
            $t = Config::get('db.'.$dbname.'.'.$table);
            if ( $t ){
                $this->schema[$table] = $t;
                $n++;
            }
        }
        Config::addClass(__CLASS__,'Loaded '.$n.' tables for ['.$dbname.']');
    }

    public function __destruct(){
        unset($this->schema);
    }

    public function getName(){ return $this->dbname;}
    public function getConnString(){ return $this->connstring;}

    public function getFieldName($table,$field){
        $r=null;
        if (isset($this->schema[$table]))
            if ( isset($this->schema[$table]['field'][$field]) )
                $r = $this->schema[$table]['field'][$field];
        return $r;
    }

    public function getTableName($table){
        $r=null;
        if (isset($this->schema[$table]))
            if ( isset($this->schema[$table]['name']) )
                $r = $this->schema[$table]['name'];
        return $r;
    }

    private function _validateOperator($oper){
        if (!isset($oper)) return '=';
        switch (strtolower(trim($oper))){
            case '=':
            case '!=':
            case '>':
            case '<':
            case '>=':
            case '<=':
            case '<>':
            case 'all':
            case 'and':
            case 'any':
            case 'not':
            case 'exists':
            case 'in':
            case 'like':
            case 'or':
            case 'is null':
            case 'unique':
                return strtoupper(trim($oper));
                break;
            default:
                return "=";
        }
    }

   /* SELECT _cosa1_, cosa2, cosa3 FROM _table [WHERE]
        cond1 [AND/OR]
        cond2 [AND/OR]
        ....
        [ORDER BY] ord [ASC/DESC]*/

    public function DMLSelect($table=null,$fields=null,$where=null,$orderby=null){
        if (!$table) return null;

        // field
        $first=true;
        $f="";
        if (isset($fields) && is_array($fields) )
            foreach($fields as $field){
                if (!$first)
                    $f .= ", ". $this->getFieldName($table,$field)."";
                else
                    $f .= "".$this->getFieldName($table,$field)."";
                $first=false;
            }
        else{
            $f="*";
        }


        // where clause
        $c="";
        if (isset($where) && is_array($where) ){
            $c.= " WHERE";
             foreach($where as $clause){
                if (!is_array($clause)) continue;
                if (!isset($clause[0]) || !isset($clause[1]) || !isset($clause[2]) ) continue;
                $field = $this->getFieldName($table,$clause[0]);
                $oper = $this->_validateOperator($clause[1]);
                $value = $clause[2];

                $c .= " ".$field." ".$oper." '".$value."'";

                if (isset($clause[3])){
                    $logicoper = $this->_validateOperator($clause[3]);
                    $c.= " ".$logicoper;
                }else
                    break;  // break loop after first clause without a logical operator
            }
        }

        // order by
        $o="";
        $first=true;
        if (isset($orderby) && is_array($orderby) ){
            $o.=" ORDER BY";
             foreach($orderby as $order){
                if (!is_array($order)) continue;
                if (!isset($order[0])) continue;
                $fr = $this->getFieldName($table,$order[0]);
                if (isset($order[1]))
                    $type = $order[1];
                if (!$first)
                    $o.=", ".$fr." ".$type;
                else
                    $o.=" ".$fr." ".$type;
                $first=false;
            }
        }

        $q = "SELECT ".$f." FROM ".$this->getTableName($table)."".$c."".$o.";";
        return $q;
    }
}

?>
