<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
/*
 * DB Connection handler, old versione will be replaced by Datasource
 */

class DBEngine implements DataBaseConnector {

    private static $errors=array();
    private static $dbpool=array();
    private static $schema=array();
    private static $lastid=null;
    private static $pconnect=0;

    public static function init(){
        // load configurations
        $conf = Config::get('DBEngine');
        if (!$conf){
            self::$errors[] = "Unable to find DBEngine configuration. (Have you loaded datasource.ini?)";
            Logger::log( __METHOD__ . " | : Unable to find DBEngine configuration. (Have you loaded datasource.ini?)" , ERR);
        }else{
            self::$pconnect = $conf['persistent'];
            if($conf['auto_connect']==true){
                $ok = self::loadAndConnect( $conf['auto_connect_dbname'] );
                if (!$ok && $conf['auto_connect_required'] ){
                    self::$errors[] = "DB Connection failed and connection is required (check datasource.ini)";
                    Logger::log( __METHOD__ . " | : DB connection failed and connection is required (check datasource.ini)" , CRIT);
                    throw new Exception("DB Connection failed and connection is required (check datasource.ini)");
                }
            }
        }
        if ( Config::$debug ){
            Config::internals('DBEngine::errors',self::$errors);
            Config::internals('DBEngine::pool',self::$dbpool);
            Config::internals('DBEngine::schema',self::$schema);
            Config::internals('DBEngine::conf',$conf);
        }
        Config::addClass(__CLASS__,'Loaded.');
    }

    public static function destroy(){
        self::closeAll();
        Logger::log(__METHOD__ . " | goodbye.",DEBUG);
    }

/*
 *  Retrieve configuration from config file (default datasource.ini but can be anything)
 *  and connect to database called "$dbname"
 *  returns null or connection ID
 */
    public static function loadAndConnect($dbname){
        $dbconf = Config::get('db.'.$dbname);
        if ( !$dbconf ){
            Logger::log( __METHOD__ . " | Configuration not found ".$dbname.")" , WARN);
            return null;
        }
            $type=( isset($dbconf['type'])) ? $dbconf['type'] : null;
            $host=( isset($dbconf['host'])) ? $dbconf['host'] : null;
            $port=( isset($dbconf['port'])) ? $dbconf['port'] : null;
            $name=( isset($dbconf['name'])) ? $dbconf['name'] : null;
        Logger::log( __METHOD__ . " | Loaded configuration [$dbname] => $type:$name.$host:$port" , DEBUG);
        $s=self::_buildConnString( $type, $host, $port, $name );
        $r=self::connect($s, $dbconf['user'], $dbconf['pass'] );
        if ($r)
            self::$schema[$r] = new Schema( $dbname, $s );
        return $r;
    }

/*
 *  Connect, read connection string, username and password and create a new PDO object
 */
    public static function connect($conn_string,$dbuser=NULL,$dbpass=NULL){
        if ( !isset($conn_string)) {
            Logger::log( __METHOD__ . " | : Missing argument: conn_string" , ERR);
            return null;
        }
        try {
            Logger::log( __METHOD__ . " | Trying PDO connection: [$dbuser:".md5($dbpass)."] @ [$conn_string] " , INFO);
            if (self::$pconnect)
                $dbh= new PDO($conn_string,$dbuser,$dbpass,array( PDO::ATTR_PERSISTENT => true) );
            else
                $dbh= new PDO($conn_string,$dbuser,$dbpass);
            self::$lastid = rand( getrandmax() / 2, getrandmax()) + rand(1,10);
            self::$dbpool[self::$lastid]=$dbh;
            Logger::log( __METHOD__ . " | Connected! CID= ".self::$lastid , DEBUG);
            return self::$lastid;
        } catch (PDOException $e){
            Logger::log( __METHOD__ . " | PDO Exception: ".$e->getMessage().")" , ERR);
            return null;
        }
    }
/*
 *  Close a specified PDO object (connection)
 */
    public static function close($id=-1){
        if (!$id) return false;
        if ($id<0) $id=self::$lastid;
        if (isset(self::$dbpool[$id])){
            self::$dbpool[$id]=NULL;
            self::$connstrings[$id]=NULL;
            unset(self::$dbpool[$id]);
            unset(self::$connstrings[$id]);
            return true;
        }
        return false;
    }
/*
 *  Close all PDO connections and remove all objects
 */
    public static function closeAll(){
        try {
            foreach (self::$dbpool as $dbh ){
                $dbh=NULL;
                unset($dbh);
            }
            return true;
        } catch (Exception $e){
            return false;
        }
    }
/*
 *  exec a query on (optionally) specified PDO object
 */
    public static function query($qry,$id=-1){
        if ($id<0) $id=self::$lastid;
        if (!$id) return null;
        if ( isset(self::$dbpool[$id]) ){
            return self::$dbpool[$id]->query($qry);
        }
        return NULL;
    }
/*
 *  Execute a query
 */
    public static function exec($qry,$id=-1){
        if ($id<0) $id=self::$lastid;
        if (!$id) return null;
        if ( isset(self::$dbpool[$id]) ){
                return self::$dbpool[$id]->exec($qry);
        }
        return NULL;
    }
/*
 *  get number of row
 */
    public static function rowCount($pdo){
        if (!$pdo) return NULL;
        return $pdo->rowCount();
    }

    public static function errorCode($pdo){
        if (!$pdo) return null;
        return $pdo->errorCode();
    }

    public static function errorInfo($pdo){
        if (!$pdo) return null;
        $e= $pdo->errorInfo();
        return $e[0];
    }

/*
 *  Fetch one row (the next one)
 */
    public static function fetch($pdo){
        if (!$pdo) return NULL;
        return $pdo->fetch(PDO::FETCH_ASSOC);
    }

/*
 *  Fetch all results as array of array (row and columns in database'table)
 */
    public static function fetchAll($pdo){
        if (!$pdo) return NULL;
        return $pdo->fetchAll(PDO::FETCH_NAMED);
    }

/*
 *  PDO->setAttribute()
 */
    public static function setAttribute(int $attribute, mixed $value, $id=-1){
        if ($id<0) $id=self::$lastid;
        if (!$id) return null;
        if ( isset(self::$dbpool[$id]) ){
            return self::$dbpool[$id]->setAttribute($attribute,$value);
        }
        return NULL;
    }

/*
 *  Begin a transaction on a (optionally) specified PDO object,
 *  select if autocommit will be true or false.
 */
    public static function beginTransaction($id=-1,$autocommit=true){
        if ($id<0) $id=self::$lastid;
        if (!$id) return false;
        if ( isset(self::$dbpool[$id]) ){
            try {
                self::$dbpool[$id]->setAttribute(PDO::ATTR_AUTOCOMMIT,$autocommit);
            }catch ( PDOException $e){
                Logger::log( __METHOD__ . " | : PDO Exception: ".$e->getMessage() , ERR);
            }
            return self::$dbpool[$id]->beginTransaction();
        }
        return false;
    }
/*
 *  Commit a previous opened transaction, $id is a PDO object in list
 */
    public static function commit($id=-1){
        if ($id<0) $id=self::$lastid;
        if (!$id) return false;
        if ( isset(self::$dbpool[$id]) )
            return self::$dbpool[$id]->commit();
        return false;
    }
/*
 *  roll back previous opened transaction, $id is a PDO object in list
 */
    public static function rollBack($id=-1){
        if ($id<0) $id=self::$lastid;
        if (!$id) return false;
        if ( isset(self::$dbpool[$id]) )
            return self::$dbpool[$id]->rollBack();
        return false;
        }
/*
 *  Human readable list of connections currently active
 *
    public static function listConnections(){
        return self::$connstrings;
    }*/
/*
 *  parse separate fields and build up a DSN string for PDO
 */
    private static function _buildConnString($dbtype='mysql',$dbhost='localhost',$dbport=NULL,$dbname=NULL) {
        $conn_string=NULL;
        if ( isset($dbtype) ) $conn_string = $dbtype.':';
        if ( isset($dbhost) ) $conn_string .= 'host='. $dbhost .';' ;
        if ( isset($dbport) ) $conn_string .= 'port='. $dbport .';' ;
        if ( isset($dbname) ) $conn_string .= 'dbname='. $dbname .'' ;
        return $conn_string;
    }

/*
 *  Uses various functions to escape special characters basing on current connection
 */
    public static function escapeStr($data,$id=-1){
 /*       if ($id<0) $id=self::$lastid;
        if ( !isset(self::$dbpool[$id]) ) return null;

        $res=null;
*/
        try{
            if ( !get_magic_quotes_gpc() )
                $res = strip_tags( addslashes($data) ,Config::get('global','allow_html_tags'));
            else
                $res = strip_tags( $data ,Config::get('global','allow_html_tags'));

            if ( is_callable("mysql_escape_string") )
                $res = mysql_escape_string($res);
            if ( is_callable("pg_escape_string") )
                $res = pg_escape_string($res);
        }catch (Exception $e){
            Logger::log( __METHOD__ . " | Escape String failed! ".$e->getMessage() , WARN);
            $res="";
        }
        return $res;
    }

    public static function select($table=null,$fields=null,$where=null,$orderby=null,$id=-1){
        if ($id<0) $id=self::$lastid;
        if (!$id || !isset(self::$schema[$id]) ) {
            Logger::log(__METHOD__ . " | FAILED, invalid connection handler: ".((!$id) ? "NULL" : $id) ,WARN);
            return null;
        }
        $q= self::query(self::$schema[$id]->DMLSelect($table,$fields,$where,$orderby));
        return self::fetchAll($q);
    }

/*
 * Build a query, using dbtables from configuration
 * Return a string, the query ready to execute or false on error
 *  $values must be an associative array in the form ( "key" => "value", ... )
 *      where "key" is an inner field name and must be equals to configuration stored into conf/db.php
 *  $table is a string, the inner table name reflecting configuration
 *  $where must be an associative array of array in the form ( "key" => array ("value", "math operator", "logical operator" ), ... )
 *      where "key" is an inner field name and must be equals to configuration
 *      math operator is any SQL math operator ( = > < LIKE ...)
 *      logical operator is any SQL logical operatator ( AND OR NOT )
 *      more complex form of sql (HAVING, IN, IS ecc...) are not really supported, but you can try if you're brave!
 */
 //TODO: expand to support more sql queries
/*
    public static function buildSelectQuery($table=null,$values=array(),$where=array() ){
            if ( count($values)==0 ) return false;
            if ( !$table ) return false;

            $qry="SELECT ";
            $tp=0;
            // iterate on every given value (key=>val)
            foreach ( $values as $key => $val){
                // if exists a field internally named key, get its value
                if ( dbfield($table,$val) ){
                    if ( $tp == 1 )
                        $qry .= ', '.dbfield($table,$val);
                    else
                        $qry .= dbfield($table,$val);
                    // write apart the values for this key
                    $insert_values[] = $val;
                    $tp=1;
                }
            }
            $qry .= " FROM ".dbtable($table)." ";
            if ( count($where)!=0){
                $qry .= " WHERE ";
                $tp=0;
                foreach ( $where as $key => $val ){
                    if ( $tp==1)
                        $qry .= " ".strtoupper($val[2])." ".dbfield($table,$key)." ".$val[1]." '".$val[0]."'";
                    else
                        $qry .= dbfield($table,$key)." ".$val[1]." '".$val[0]."'";
                    $tp=1;
                }
                $qry .= ";";
            }
            return $qry;
    }

    public static function buildInsertQuery($table=null,$values=array()){
            if ( count($values)==0 ) return false;
            if ( !$table ) return false;

            $qry="INSERT INTO ".dbtable($table)." (";
            $insert_value=array();
            $tp=0;
            // iterate on every given value (key=>val)
            foreach ( $values as $key => $val){
                // if exists a field internally named key, get its value
                if ( dbfield($table,$key) ){
                    if ( $tp == 1 )
                        $qry .= ', '.dbfield($table,$key);
                    else
                        $qry .= dbfield($table,$key);
                    // write apart the values for this key
                    $insert_values[] = $val;
                    $tp=1;
                }
            }
            $qry .= ") VALUES(";
            $tp=0;
            // iterate every value and insert it
            foreach ( $insert_values as $val ){
                if ( $tp==1)
                    $qry .= ", '".$val."'";
                else
                    $qry .= "'".$val."'";
                $tp=1;
            }
            $qry .= ");";
            return $qry;

   }
*/
    /* $where must be an array of array in the form -> ( "fieldname" => array( "val", "mathoperator", "logicaloperator" ) )
        where mathoperator is one of SQL math operator
        and logical operator is one of AND, OR, NOT, ...
    */
/*    public static function buildUpdateQuery($table=null,$values=array(), $where=array()){
            if ( count($values)==0 ) return false;
            if ( !$table ) return false;

            $qry="UPDATE ".dbtable($table)." SET ";
            $tp=0;
            // iterate on every given value (key=>val)
            foreach ( $values as $key => $val){
                // if exists a field internally named key, get its value
                if ( dbfield($table,$key) ){
                    if ( $tp == 1 )
                        $qry .= ', '.dbfield($table,$key)."='".$val."'";
                    else
                        $qry .= dbfield($table,$key)."='".$val."'";
                    $tp=1;
                }
            }
            $qry .= " WHERE ";
            $tp=0;
            // iterate every value and insert it
            foreach ( $where as $key => $val ){
                if ( $tp==1)
                    $qry .= " ".strtoupper($val[2])." ".dbfield($table,$key)." ".$val[1]." '".$val[0]."'";
                else
                    $qry .= dbfield($table,$key)." ".$val[1]." '".$val[0]."'";
                $tp=1;
            }
            $qry .= ";";
            return $qry;
    }

    public static function buildDeleteQuery($table=null, $where=array()){
            if ( !$table ) return false;

            $qry="DELETE FROM ".dbtable($table)." WHERE ";
            $tp=0;
            // iterate every value and insert it
            foreach ( $where as $key => $val ){
                if ( $tp==1)
                    $qry .= " ".strtoupper($val[2])." ".dbfield($table,$key)." ".$val[1]." '".$val[0]."'";
                else
                    $qry .= dbfield($table,$key)." ".$val[1]." '".$val[0]."'";
                $tp=1;
            }
            $qry .= ";";
            return $qry;
    }

}
*/
#Examples:

#echo DB::build_update_query('auth',array("id"=>"1","user"=>"test","pwd"=>"pass","cat"=>"1"), array("user"=> array("pippo","LIKE",""), "id"=> array("5",">","or") ) );
#echo DB::build_delete_query('auth',array("user"=> array("pippo","LIKE",""), "id"=> array("5",">","or") ) );
#echo DB::build_select_query("auth", array("id","cat"),array( "user" => array("test","=",""), "pwd"=> array("test","=","AND") ));
#echo DB::build_insert_query('auth',array("id"=>"1","user"=>"test","pwd"=>"pass","cat"=>"1") );
}


//#################################################################################################
//aliases:



?>
