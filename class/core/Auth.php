<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */

final class Auth{

    private static $config;
    private static $loginfunction;
    private static $enabled;

    public final static function init(){
        self::$config = Config::get(__CLASS__);
        self::$enabled = self::$config['enabled'];

        if (!self::$enabled){
            Logger::log(__METHOD__ . " | not enabled",NOTICE);
            return false;
        }

        self::$loginfunction = self::$config['endpoint'] . "::login";

        try{

            // check if method exists (is callable)
            $method = explode("::",self::$loginfunction);
            if ( !is_callable($method))
                throw new Exception("Configured endpoint method [".$method[0]."::".$method[1]."] not found");

            if (self::$config['endpoint'] != __CLASS__ ){    //avoid reentrant init() calls
                // call $backend::init()
                $method = array(self::$config['endpoint'],"init");
                if ( !is_callable($method))
                    throw new Exception("Configured endpoint ".$method[1]."] not found (failed to init)");
                $ret=forward_static_call_array($method,  array() );      // $backend::init()
            }else
                self::$loginfunction = 'Auth::dummy';


        }catch (Exception $e){
            Logger::log(__METHOD__ . " | ".$e->getMessage(),WARN);
            Logger::log(__METHOD__ . " | Fallback to [Auth::dummy()] endpoint",INFO);
            self::$loginfunction = 'Auth::dummy';
        }
        Logger::log(__METHOD__ . " | Endpoint: ".self::$loginfunction,INFO);
        Config::addClass(__CLASS__,' Endpoint: '.self::$loginfunction);
        return true;
    }

    public final static function destroy(){

    }

    public final static function username(){

        if (!self::$enabled){
            Logger::log(__METHOD__ . " | Auth is currently disabled.",WARN);
            return false;
        }

        if ( self::logged() ) return Config::get('session','realname');
        return "";
    }

    public final static function uid(){

        if (!self::$enabled){
            Logger::log(__METHOD__ . " | Auth is currently disabled.",WARN);
            return false;
        }

        if ( self::logged() ) return Config::get('session','uid');
        return "";
    }

    public final static function logged(){

        if (!self::$enabled){
            Logger::log(__METHOD__ . " | Auth is currently disabled.",WARN);
            return false;
        }

        if ( Config::get('session','logged')!=null && Config::get('session','uid')!=null ){
            Logger::log(__METHOD__ . " | Session: [".Config::get('session','uid')."] [".Config::get('session','logged')."] ",DEBUG);
            return true;
        }
        Logger::log(__METHOD__ . " | no active session",DEBUG);
        return false;
    }

    // an userdata array formatted as( 'username', 'md5hashpwd' );
    // a loginfunction that accept tow params (user,passwd) and return an array of field=>values or boolean false on fail
    public final static function login($userdata=array() ){

        if (!self::$enabled){
            Logger::log(__METHOD__ . " | Auth is currently disabled.",WARN);
            return false;
        }

        if ( self::logged() === true ){
            Logger::log(__METHOD__ . " | User already authenticated",NOTICE);
            return true;                                            // Check if user is already logged in
        }
        if ( !isset($userdata[0]) || !isset($userdata[1]) ){
             Logger::log(__METHOD__ . " | Invalid or void arguments ",WARN);
             return false;               // check if passed arguments is sane
        }

        $md5pwd = $userdata[1];
        $uname= $userdata[0];

        Logger::log(__METHOD__ . " | Search [$uname] with password [$md5pwd] ",DEBUG);

        $method = explode("::",self::$loginfunction);

        if ( !is_callable($method)){
            Logger::log(__METHOD__ . " | Invalid callable [".$loginfunction."] ",ERR);
            return false;
        }

        $realname = "";
        $ret=forward_static_call_array($method, array($uname,$md5pwd,&$realname) );

        if ( $ret === false ){
            Logger::log(__METHOD__ . " | Authentication failed",NOTICE);
            return false;
        }

        $uid = str_replace(" ","_",$uname);
        Logger::log(__METHOD__ . " | Say welcome to [".$uid."] ",INFO);

        $savedvar=array();
        foreach( $ret as $fieldname => $fieldval ){
            Config::setSession($fieldname,$fieldval);
            $savedvar[]=$fieldname;
        }
        Config::setSession('fields',$savedvar);
        Config::setSession('uid',$uid);
        Config::setSession('logged','1');
        Config::restoreSession();       // force reload session variables
        return true;
    }

    public final static function logout(){

        if (!self::$enabled){
            Logger::log(__METHOD__ . " | Auth is currently disabled.",WARN);
            return false;
        }

        if ( !self::logged() ){
            Logger::log(__METHOD__ . " | Invalid call to logout() no active session.",WARN);
            return false;
        }
        Logger::log(__METHOD__ . " | Say goodbye to [".Config::get('session','uid')."] ",INFO);
        $savedvar=Config::get('session','fields');
        if ( is_array($savedvar) )
            foreach( $savedvar as $var){
                unset ($_SESSION[$var]);
                Config::del('session',$var);
            }
        unset ($_SESSION['fields']);
        unset ($_SESSION['uid']);
        unset ($_SESSION['logged']);
        Config::del('session','fields');
        Config::del('session','uid');
        Config::del('session','logged');
        session_destroy();
        return true;
    }

    public final static function dummy($uname=null,$upwd=null){
        if (self::$config['default'])
            return true;
        return false;
    }

}

?>
