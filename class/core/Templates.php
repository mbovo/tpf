<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software; 
 * you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation; 
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
 
 
// Load the default template
final class Templates {

	private static $config=array();
	private static $tname = "";
	private static $obj = null;

	public static function init(){
		self::$config = Config::get('Templates');
		self::$tname = self::$config['default'];
		
		$tname = self::$tname;
		$path = self::$config['root'] . '/' . $tname;
		
		if ( !is_readable($path) ){
			Logger::log(__METHOD__ . " | invalid path $path ",WARN);
			Config::addClass(__CLASS__,' FAIL');
			return false;
		}
		$path .= '/' . $tname . '.php';
		if ( !is_readable($path) ){
			Logger::log(__METHOD__ . " | file not found $path ",WARN);
			Config::addClass(__CLASS__,' FAIL');
			return false;		
		}
		include_once($path);
		
		$cl = get_declared_classes();
		if ( in_array($tname,$cl)){
			self::$obj = new $tname();
			Logger::log(__METHOD__ . " | Created new object [$tname] ",INFO);
		}else{
			Logger::log(__METHOD__ . " | cannot create new class [$tname] ",ERR);
			self::$obj = new Template();
		}

		Config::addClass(__CLASS__,' OK');
	}

	public static function destroy(){
		// you cannot do an unset(obj) 'cause is static
		self::$obj->__destruct();
	}
	
	public static function get(){
		return self::$obj;
	}
	
	public static function name(){
		return self::$tname;
	}
	
	public static function path(){
			return '/templates/' . Templates::name()  ;
	}
	
	public static function getPath($content,$name,$ext){
		$path = str_replace(array('.','/'), '', self::$config['root']);

		if ( !in_array( $content, self::$config['allowed_contents']) ){
			Logger::log(__METHOD__. " | Unallowed content was requested [$content] ");
			return false;
		}
		if ( !in_array( $ext, self::$config['allowed_extension']) ){
			Logger::log(__METHOD__. " | Unallowed extension for $content was requested [$name.$ext] ");
			return false;		
		}

		$path .= '/' . self::$tname . '/' . str_replace( '/','',$content)  . '/' . str_replace(array('/','.'), array('','/'),$name) . '.' . $ext;
		#$weblink = Config::get('app','webroot') . '/' .$path; 
		$weblink = Config::get('app','webroot') . '/templates/' . Templates::name() . '/' . str_replace( array('/','.'),'',$content) . '/' . str_replace(array('/','.') ,array('','/'), $name)  . '.' . $ext; 
		$path = Config::internals('basepath') . '/' . $path;
		if ( file_exists( $path ) )
			return $weblink;
		else
			Logger::log(__METHOD__ . " | file not found [$path] [$weblink]",WARN);
		return false;
	}

}

?>
