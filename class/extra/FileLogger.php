<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
/**
 * A file logger
 */

class FileLogger extends LoggerFacility{

    private $logpath = "";
    private $fp = null;

    public function __construct($level,$enabled=true){
        parent::__construct($level,$enabled);

        $conf = Config::get(__CLASS__);

        if ( !$enabled ) return null;

        // subconfiguration of level
        $newlevel = Logger::$errorval[ strtolower( $conf['level'] ) ];
        if ( $level !=  $newlevel ){
            $this->setLevel($newlevel);
        }

        $this->logpath = $conf['file_path'];
            if ( !file_exists ( $this->logpath ) || !is_writeable( $this->logpath ) ) {
                ErrorHandler::forceEcho("<!-- Logger::FatalError - Invalid or not writeable log path ".$conf['file_path']." -->");
                $this->enabled=false;
                return null;
            }
            if ( $conf['file_rotate'] )
                $this->logpath .= '/' . date( $conf['file_format'] ) . '_' . $conf['file_name'];
            else
                $this->logpath .= '/' . $conf['file_name'];

            if ( ! touch( $this->logpath ) ) {
                ErrorHandler::forceEcho("<!-- Logger::FatalError - Cannot write to log file ".$this->logpath." -->");
                $this->enabled=false;
                return null;
            }

            try{
                $this->fp = fopen($this->logpath, "a");
            }catch (Exception $e){
                ErrorHandler::forceEcho("Critical: cannot open log file ".$this->logpath. " disabling Logger");
                $this->enabled=false;
                return null;
            }
            $this->log(__METHOD__ . " | Level set to " . $this->humanserror[$this->level] , INFO );
        Config::addClass(__CLASS__,'level: '.$this->level.' | '."logpath: ".$this->logpath);
    }

    public function __destruct(){
        if ( !$this->enabled ) return;
        #$this->log(__METHOD__, INFO );
        #error_log( "        \n", 3, self::$logpath );
        #fwrite($this->fp, "         \n");
        fclose($this->fp);
    }

    public function log($value,$prio=NOTICE) {
        if ( !$this->enabled ) return;
        #if ( $prio <= self::$level && self::$level > 0)    syslog($prio,$value."\n");
        if ( $prio > $this->level || $this->level == DISABLED ) return false;

        $s=date("Y-m-d H:i:s (T) ") . "|" . $_SERVER['REMOTE_ADDR'] . "|" . $this->humanserror[$prio] . " :> " . $value . "\n";
        try{
            if ( fwrite($this->fp,$s) === false) throw new Exception($s);
        }catch (Exception $e){
            ErrorHandler::forceEcho("Critical: cannot write to log file ".$s. "[".$e->getMessage()."]");
        }

    }

    public function setLevel($value){
        if ( !$this->enabled ) return;
        if ( $value<=DEBUG && $value>=EMERG ) {
            $this->level = $value;
            $this->log(__METHOD__ . ' - Log level is now '. $this->humanserror[$value] ,INFO);
        }
    }

}

?>
