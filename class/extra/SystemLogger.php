<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
/**
 * A syslog (or windows nt) logger
 *
 */


class SystemLogger extends LoggerFacility{

    private $ident;

    public function __construct($level,$enabled=true){
        parent::__construct($level,$enabled);

        $conf = Config::get(__CLASS__);

        if ( !$enabled ) return null;

        // subconfiguration of level
        $newlevel = Logger::$errorval[ strtolower( $conf['level'] ) ];
        if ( $level !=  $newlevel ){
            $this->setLevel($newlevel);
        }

        $server = Config::internals('SERVER');
        $this->ident = $conf['ident'];

        openlog( $this->ident . " - (". $server['REMOTE_ADDR'] .")  ", LOG_NDELAY , LOG_USER );

        $this->log(__METHOD__ . " | Level set to " . $this->humanserror[$this->level] , $this->level );
        Config::addClass(__CLASS__,'level: '.$this->level.' | '."using syslog");
    }

    public function __destruct(){
        if ( !$this->enabled ) return;
        closelog();
    }

    public function log($value,$prio=NOTICE) {
        if ( !$this->enabled ) return;
        if ( $prio > $this->level || $this->level == DISABLED ) return false;

        syslog(  $prio , $this->humanserror[$prio] . " :> " . $value . "\n");
    }

    public function setLevel($value){
        if ( !$this->enabled ) return;
        if ( $value<=DEBUG && $value>=EMERG ) {
            $this->level = $value;
            $this->log(__METHOD__ . ' - Log level is now '. $this->humanserror[$value] ,INFO);
        }
    }

}

?>
