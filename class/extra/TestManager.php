<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software; 
 * you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation; 
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
 
class TestManager implements ManagerController{

	public static function get(array $params=null){
		$me = __CLASS__;
		$met = __METHOD__;
		ErrorHandler::forceEcho("Hey this is <b>$me</b> on method <b>$met</b> with params <em>".print_r($params,true)."</em>");
		ErrorHandler::forceEcho("<br>You can Implement your personal ManagerController to print this");
		return true; //MUST
	}


}

?>
