<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
/* How to use:
 *   $filename = PluginManager::exec('Captcha','generate',array( $chars ) );
 *   $filename = PluginManager::exec('Captcha','generate', null );
 *
 * $filename will be the path of the result image, ready to html output
 * $chars is the argument of the function
*/

class Captcha extends Plugin{

    private $stop=false;

    public function __construct(){
        $this->p_name = 'Stupid Captcha Generator';
        $this->p_version = '0.2';
        if ( !is_callable('imagecreatefromjpeg') ){
            $this->stop = true;
            throw new Exception("GD library not found, please load/compile gdlib for php");
        }
    }

    /* Generate a captcha image and return its path (ready to output)
     *   chars = how many chars to insert (default 5)
     *   session_var = the name of session variable where the value is stored
     */
    public function generate($chars=5,$session_var='CAPTCHA_CHECK'){
        if ($this->stop){
            throw new Exception("GD library not found, please load/compile gdlib for php");
            return false;
        }

        if ( $chars > 10 ) $chars = 10;

        if ( $chars <= 5 ) $img="160x53.jpg";
        else $img="320x107.jpg";

        $stringa = md5(microtime()+rand() * microtime() + rand(1,200));
        $risultato = substr($stringa, 0, $chars);
        $immagine = imagecreatefromjpeg( Config::get('Plugins','root') . "/Captcha/data/".$img);
        $testo = imagecolorallocate($immagine, rand(1,200), rand(1,200), rand(1,200));

        for( $i=0; $i<strlen($risultato); $i++)
            imagettftext( $immagine , 32 , rand(-15,45) , 18+(30*$i) , 40+rand(0,10) , 0 , Config::get('Plugins','root') . "/Captcha/data/".rand(1,3).".ttf" , substr($risultato,$i,1) );

        Config::setSession($session_var,$risultato);

        $partial = '/img/tmp/'.Config::internals('sid').'.jpg';
        $fname = Config::internals('basepath') . Templates::path() . $partial;
        $sname = Config::get('app','webroot') . Templates::path() . $partial;
        imagejpeg($immagine,$fname);
        #echo '<img src="'.$fname.'">';
        return $sname;

    }

    public function check($value,$session_var='CAPTCHA_CHECK'){
        if ($this->stop){
            throw new Exception("GD library not found, please load/compile gdlib for php");
            return false;
        }
        if ( !$value || !$session_var || ! Config::get('session',$session_var) ) return false;
        if ($value==Config::get('session',$session_var))
            return true;
        return false;
    }
}

?>
