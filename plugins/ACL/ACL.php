<?php
/*
 * This file is part of <TPF - Tiny PHP Framework>
 *
 * Copyright (C) 2011 - Manuel Bovo
 *
 * <TPF - Tiny PHP Framework> is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2 of the License, or (at your option) any later version.
 *
 * TPF> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
// just in case
Config::set('1','Plugins.ACL','static');
Config::loadConfFile('acl'); // load definition lists

class ControlManager extends Plugin{

    private static $acl=array();

        // base function
    public static function canDoActionAndLog($who,$action,$logsucc=true,$logfail=false){
        // Root can do everything
        if ( in_array( Perms::ROOT, self::$acl[ $who ] ) )
            return true;
        // Void user can do nothing
        if ( in_array( Perms::VOID, self::$acl[ $who ] ) )
            return false;

        if ( in_array($action, self::$acl[ $who ]) ){
            if ( $logsucc )
                logSuccess("ACL::canDoAction()"," User ".conf('username')." has ".Perms::tostring($action));
            return true;
        }
        if ( $logfail )
            logFail("ACL::canDoAction()"," User ".conf('username')." asked for ".Perms::tostring($action)." : DENIED!");
        return false;
    }

    // Whitout logging
    public static function canDoAction($who, $action ){
        return self::canDoActionAndLog($who, $action, false,false );
    }

    // Whitout logging and ask for me
    public static function canIDo($action){
        return self
/*
 * call a function if $action can be executed by $who
 *
 * $action must be a Perms constant and should be in acl list for who type
 * $pointer should be not null
 * $args must be an array of arguments for $pointer()
 */
    public static function call($who,$action,$pointer=null,$args=null){
        if ( $pointer!=null && is_callable($method) ) {

            if ( self::canDoAction($who,$action) ){                             // check ACL
                if ( isset($args) && is_array($args) )                          // if there are arguments
                    return call_user_func_array($pointer,$args);
                else
                    return call_user_func($pointer);
                logSuccess("ACL::call() PASSED | ".$who.", ".Perms::tostring($action).",".$pointer."()");
            }else                                                               // Access denied!
                logFail("ACL::call() DENIED | ".$who.", ".Perms::tostring($action).", ".$pointer."()");

        }else                                                                   // invalid function pointer
            logError("ACL::call() ERROR: Invalid function pointer.");
        return null;
    }

    public static function callStatic($who,$action,$pointer=null,$args=null){
        if ( $pointer!=null && is_callable($method) ) {

            if ( self::canDoAction($who,$action) ){                             // check ACL
                if ( isset($args) && is_array($args) )                          // if there are arguments
                    return forward_static_call_array($pointer,$args);
                else
                    return forward_static_call($pointer);
                logSuccess("ACL::callStatic() PASSED | ".$who.", ".Perms::tostring($action).",".$pointer."()");
            }else                                                               // Access denied!
                logFail("ACL::callStatic() DENIED | ".$who.", ".Perms::tostring($action).", ".$pointer."()");

        }else                                                                   // invalid function pointer
            logError("ACL::callStatic() ERROR: Invalid function pointer.");
        return null;
    }


/*
 * Initialize Logger, sets up acl list from configuration file, this method must be called BEFORE any other of this class
 */
    public static function init(){
        self::$config = Config::get('ACL');
    }

    public function __construct(){
        $this->p_name = 'ACL Control Manager';
        $this->p_version = '0.1';
    }

}

?>
