<?php

$t = Templates::get();  # read the default template
//$t->putHeader(); # not required, done by constructor

#using a local variable
$webroot = Config::get('app','webroot');

$title=array('#',Config::get('app','name'));
$links=array(
    'Home' => '#',
    'About' => '#',
    'Contacts' => '#',
    'Dropdown' => array(
                        'Action' => '#',
                        'Another action' => '#',
                        'somethin' => '#',
                        '-' => '-',
                        'Nav Header' => '-',
                        'First' => '#',
                        'Second' => '#'
                  )
);
$form = '
          <form class="navbar-form navbar-right" role="form">
            <div class="form-group">
              <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
          </form>

';

$t->putNavbar($title,$links,$activelink=0,$dark=true,$fixed=true,$static=false,$form,$tostring=false);

$t->out('
  <div class="container theme-showcase" role="main">
');

Auth::login( array('test','123124124123123123') );
Auth::logout();

$t->putJumbo('Hallo, World!','This is TPF: Tiny Personal Framework, a easy to use, lightwave PHP framework let you build
your presentation layer in few, automated steps<br/>
Provide some functions and object in order to integrate easily with datbase, configuration files, custom plugins and templates.
The template in this page is a very basic one, using Bootstrap Css to render the page',$tostring=false);

$names=array('Default','Primary','Success','Info','Warning','Danger','Link');

$t->putTitle('Buttons');
$t->out('<p>');
for($i=0;$i<7;$i++)
    $t->putButton($names[$i],$type=0,$size=3,$level=$i);
$t->out('</p>');
$t->out('<p>');
for($i=0;$i<7;$i++)
    $t->putButton($names[$i],$type=0,$size=0,$level=$i);
$t->out('</p>');
$t->out('<p>');
for($i=0;$i<7;$i++)
    $t->putButton($names[$i],$type=0,$size=2,$level=$i);
$t->out('</p>');
$t->out('<p>');
for($i=0;$i<7;$i++)
    $t->putButton($names[$i],$type=0,$size=1,$level=$i);
$t->out('</p>');

$t->out('
      <div class="page-header">
        <h1>Tables</h1>
      </div>
      <div class="row">
        <div class="col-md-6">
          <table class="table">
            <thead>
              <tr>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Username</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Larry</td>
                <td>the Bird</td>
                <td>@twitter</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="col-md-6">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Username</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Larry</td>
                <td>the Bird</td>
                <td>@twitter</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Username</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td rowspan="2">1</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
              </tr>
              <tr>
                <td>Mark</td>
                <td>Otto</td>
                <td>@TwBootstrap</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
              </tr>
              <tr>
                <td>3</td>
                <td colspan="2">Larry the Bird</td>
                <td>@twitter</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="col-md-6">
          <table class="table table-condensed">
            <thead>
              <tr>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Username</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
              </tr>
              <tr>
                <td>3</td>
                <td colspan="2">Larry the Bird</td>
                <td>@twitter</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
');

$hat=array('h1','h2','h3','h4','h5','h6','p');
$t->putTitle('Labels');
for ($k=0;$k<7;$k++){
    $t->out('<'.$hat[$k].'>');
    for($i=0;$i<5;$i++)
        $t->putLabel($names[$i],$level=$i,$dimension=0);
    $t->out('</'.$hat[$k].'>');
}

$t->putTitle('Badges');
$t->out('<p>');
$t->putBadge(array('Inbox','#','42'));
$t->out('
      </p>
      <ul class="nav nav-pills">
        <li class="active">'.$t->putBadge(array('Home','#','42'),$tostring=true).'</li>
        <li>'.$t->putBadge(array('Profile','#','0'),$tostring=true).'
        <li>'.$t->putBadge(array('Messages','#','3'),$tostring=true).'
      </ul>
');

$t->out('      <div class="page-header">
        <h1>Dropdown menus</h1>
      </div>
      <div class="dropdown theme-dropdown clearfix">
        <a id="dropdownMenu6" href="#" role="button" class="sr-only dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu6">
          <li class="active" role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
          <li role="presentation" class="divider"></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
        </ul>
      </div>
');

$t->putTitle('Navs');
$selinks=array('Home'=>'#','Profile'=>'#','Messages'=>'#');
$t->putNav($selinks,$activelink=0,$tab=true,$pill=false,$tostring=false);
$t->putNav($selinks,$activelink=0,$tab=false,$pill=true,$tostring=false);


$t->putTitle('Navbars');
$t->putNavbar($title,$links,$activelink=0,$dark=false,$fixed=false,$static=false,$form=null,$tostring=false);
$t->putNavbar($title,$links,$activelink=0,$dark=true,$fixed=false,$static=false,$form=null,$tostring=false);

$t->putTitle('Alerts');
$t->putAlert('Well done!','You successfully read this important alert message',$level=0,$tostring=false);
$t->putAlert('Heads up!','This alert needs your attentions, but its not super important',$level=1,$tostring=false);
$t->putAlert('Warning','Best check yourself',$level=0,$tostring=false);
$t->putAlert('Oh Snap!','Change a few thins up and try again',$level=0,$tostring=false);

$t->putTitle('Progress bars');
$t->putProgressBar($val=60,$min=0,$max=100,$text='Complete',$level=0,$tostring=false);
$t->putProgressBar($val=40,$min=0,$max=100,$text='Complete',$level=1,$tostring=false);
$t->putProgressBar($val=20,$min=0,$max=100,$text='Complete',$level=2,$tostring=false);
$t->putProgressBar($val=60,$min=0,$max=100,$text='Complete',$level=3,$tostring=false);
$t->putProgressBar($val=80,$min=0,$max=100,$text='Complete',$level=4,$tostring=false);
$t->putProgressBar($val=60,$min=0,$max=100,$text='Complete',$level=5,$tostring=false);

$t->putTitle('List groups');
$lista=array('Cras justo odio','Dapibus ac facilisis in','Morbi leo risus','Porta ac consectetur ac','Vestibulum at eros');
$lista2=array('Cras justo odio'=>'#','Dapibus ac facilisis in'=>'#','Morbi leo risus'=>'#','Porta ac consectetur ac'=>'#','Vestibulum at eros'=>'#');
$lista3=array(
            array('#','List group item heading','Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.'),
            array('#','List group item heading','Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.'),
            array('#','List group item heading','Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.')
            );
$data=array(
    $t->putList($lista,$active=0,$linked=false,$generic=false,$tostring=true),
    $t->putList($lista2,$active=0,$linked=true,$generic=false,$tostring=true),
    $t->putList($lista3,$active=0,$linked=false,$generic=true,$tostring=true),
);
$t->putRowMediumColumn($data,$columns=3,$tostring=false);

$t->putTitle('Panels');
$panel=array('title'=>'Panel Title','body'=>'panel content');

$data=array(
    $t->putPanel($panel,$level=0,$tostring=true),
    $t->putPanel($panel,$level=1,$tostring=true),
    $t->putPanel($panel,$level=2,$tostring=true),
    $t->putPanel($panel,$level=3,$tostring=true),
    $t->putPanel($panel,$level=4,$tostring=true),
    $t->putPanel($panel,$level=5,$tostring=true),
);
$t->putRowMediumColumn($data,$columns=3,$tostring=false);

$t->putTitle("Wells");
$t->out('
      <div class="well">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed diam eget risus varius blandit sit amet non magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur.</p>
      </div>
');

$t->out('    </div> <!-- /container -->');

//######################The footer ########################################################
$t->putFooter();

?>
