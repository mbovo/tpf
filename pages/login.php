<?php

$t = Templates::get();	# read the default template
//$t->putHeader(); # not required, done by constructor

#using a local variable
$webroot = Config::get('app','webroot');

$t->out('

   <div class="container">

      <form class="form-signin" role="form">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="user" class="form-control" placeholder="Username" required autofocus>
        <input type="password" class="form-control" placeholder="Password" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->
');

$t->putFooter();


?>
