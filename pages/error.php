<?php

header('HTTP/1.0 404 Not Found');

$t = Templates::get();
$menu=array(
	
);

$title=array('#',Config::get('app','name'));
$links=array(
	Lang::get('homepage') => PageManager::getURL('home'),
);
$t->putNavbar($title,$links,$activelink=0,$dark=true,$fixed=true,$static=false,$form=null,$tostring=false);

$imgname = Config::get('PageManager','eIMG');
$etext = Config::get('PageManager','eTEXT');
$etitle = Config::get('PageManager','eTITLE');

$t->out('
	 <div class="jumbotron error-template">
        <h1>'.lang($etitle).'</h1>
        <img src="'.$t->getPath('img',$imgname,'png').'">
     <div>
     <div class="container">
		  <div class="alert alert-danger" role="alert">
		    <strong>'.lang($etext).'</strong>
		  </div>
      </div>
');

$t->putFooter();

?>
