;<?php __halt_compiler(); ?>


[ACL]
; Integer values of differents access levels
level[] = 0
level[] = 10
level[] = 20
level[] = 30
level[] = 100
level[] = 200
level[] = 255

; Numerical level to Human readable mapping (in order)
level_map[] = "Null"
level_map[] = "Read"
level_map[] = "Write"
level_map[] = "Access"
level_map[] = "Grant Permission"
level_map[] = "Edit Config"
level_map[] = "SuperAdmin"


; groups: each group can list one or more levels of access
groups[]=void
groups[]=admin
groups[]=roots


[acl_group.void]
level[] = 0

[acl_group.admin]
level[] = 10
level[] = 20
level[] = 30
level[] = 100

[acl_group.root]

level[] = 255
