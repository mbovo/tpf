;<?php __halt_compiler(); ?>

[Logger]
enable = 1                      ; disable all logging (?)
facility = FileLogger           ; class name of a child of LoggerFacility (your real logger class)
level = debug                   ; current logging level, deafult is "notice"
                                ;       values -> debug, info, notice, warning, error, critical, alert, emergency
                                ;       "info" is a debugging level and "debug" is a verbose! level be carefull

[FileLogger]
level = debug
file_path = "./logs"            ; log path must be writeable by web server (not for syslog)
file_name = "general.log"
file_rotate = 1                 ; write date in file name?
file_format = "Y_m_d"           ; date format (passed to date() function see php reference )

[SystemLogger]
ident = TPF              ; ident name to use on syslogger
level = debug
