;<?php __halt_compiler(); ?>
; For Database sources

;# DATABASE CONNECTION  can be mysql, pgsql, sqlite or any other valid DSN prefix for PHP DBO connection
[Datasource]
enabled = 0
; Connect automatically on startup?
auto_connect = 0
; Connection is required (ie: fatal error on fail)
auto_connect_required = 0
; DBname of default connection
auto_connect_dbname = apollo
; Name of the subclass to use here
auto_connect_class = DB

; List of DB configuration to load
dbnames[] = default
dbnames[] = apollo
;dbnames[] = anothertest

; DB configuration
[db.default]
type = mysql
host = localhost
;port = 3306
user = tester
pass = password
name = dbname
persistent = 1

[db.apollo]
type = mysql
host = host
port = 3306
user = test
pass = test
name = data
persistent = 1


;#Example:
;[db.my_db_name]    ; my_db_name MUST be the same string used in dbnames[] array before
;type = mysql|postgres|odbc|mssql|sqlite
;host = localhost                                       ; fqdn or IP
;port = 3306                                            ; can be omitted
;user = user
;pass = password
;name = dbname
;persistent = 1                                         ; 1|0 reusing PDO connection?
