;<?php __halt_compiler(); ?>
;This is the main configuration file for TPF
; here you can found every type of configuration variables for
; class loading and general behaviour of TPF
; is like a php.ini and it is parsed by a parse_ini_file() call
; THIS FILE IS REQUIRED AND YOU CANNOT CHANGE ITS NAME WITHOUT MODIFY TPS class!

[global]
debug = 1                           ; debug mode?
session_timeout = 3600              ; PHP_SESSION default timeout
default_timezone = "Europe/Rome"    ; default timezone for date() and similar
allow_html_tags = ""                ; allowd html tags when parsing input

; ########################### CLASS Configuration (by name) #########################################################
[Config]
persistant = 0                      ; Config is persistant across pages using PHP_SESSIONS ?    #DEPRECATED BUGGED
persistant_safe_list = 0            ; List of safe functions can be saved in sessions           #DEPRECATED BUGGED

include[] = "logger.php"            ; logger conf
include[] = "app.php"               ; application conf
include[] = "datasource.php"        ; database conf
include[] = "auth.php"              ; auth backend
;include[] = "hereyourconf.php"     ; any other conf file stored in ./data/conf/

[ErrorHandler]
silence_output = 0              ; silencing everything also echo and print* family functions
silence_errors = 0              ; silence error ie dont write it to standar web server logs
dump = 0                        ; dump every configuration in debug mode to a file
dumpdir = "./logs"              ; WARN: this is done for each page requested by the client, one per session!!!
dump_file_format = "Y_m_d"      ; files will be named $SESSIONID_$(dump_file_format).dump like: slb8jkh14f0sp0tpf361b45em5_2013_08_12-13_04.dump

[Lang]
ask_browser = 1                 ; ask the preferred language to the user's browser
default = en                    ; default language if not found or error

; ########################### PHP directives #########################################################################
; here you can re-define any type of php.ini settings TPF will try to apply theme calling ini_set()
; YOU MUST TYPE THE VALUE WITHN DOUBLE STRIP
[phpini]
display_errors = "on"

