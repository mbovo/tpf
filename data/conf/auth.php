;<?php __halt_compiler(); ?>



[Auth]
enabled = 0                      ; disable authentication backend
endpoint = AuthTest              ; class used to authenticate

[AuthTest]
default = true                  ; the login method should fail or grant access to everyone?

[AuthSQL]
; this module will authenticate agains a SQL hinstance (require Datasource class to be configured properly)
; See datasource.php, this refers to a specific [DB.xxxx] section where xxxx is your the value of db_dsn here
db_dsn = apollo
; the name of the table where auth info are stored
db_table = users
; the fields retrieved from DB
db_get_field[] = realname
db_get_field[] = shell
db_get_field[] = homedir
db_get_field[] = mail_domain
;db_get_field[] = yourfield... will be stored in session variables

db_auth_user = user
db_auth_password = password
;db_auth_method = md5
db_auth_method = plain

;optional where clause used for authentication
;db_auth_optional = "active_web = '1'"
