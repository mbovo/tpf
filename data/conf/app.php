;<?php __halt_compiler(); ?>
;; Configuration file for application
;; like php.ini syntax

;This file is required!

; Application config (ie: what is displaied, not the internals values)
[app]
webroot = "/tpf"
title = "TPF"
name = "Tiny Personal Framework"
version = "3.0"
description = "A tiny and smart PHP Framework"
copyright = "Copyright &copy; 2010-2014"
authorname = "Manuel Bovo"
; this is the error message print in case of unrecoverable error BEFORE loading pages
; %reason% will be expand with the text message given by code
; %title% will be expanded with the app.title value
error_msg[] = "<!DOCTYPE html><html><head><title> %title% </title><body>"
error_msg[] = "<h2>Oops, this is an exception!</h2>"
error_msg[] = "<h4> %reason% </h4>"
error_msg[] = "<em>Please Retry or contact an administrator</em>"
error_msg[] = "</body></html>"
mask_exception = 0  ; throw exception text or try to present present a user like page (the error page)?

; TPF use a FrontController Pattern to serve contents here configurable
; these are the default class, method and params called when no argument nor URI given
; The PageManager act like the old Page Controller pattern
default_manager = "PageManager"
default_method = "home"
default_params[] = ""

standard_menu["Home"] = "#pagewidth"
standard_menu["Sala Prove"] = "#Sala"
standard_menu["Strumenti"] = "#Strumenti"
standard_menu["S.M.G."] = "#smg"
standard_menu["About"] = "#footer"

[PageManager]
root = "./pages"
; can be black | white | off
; blacklist -> access to the pages listed will be denied
; whitelist -> access to pages not in list will be denied
use_list = black
list[] = _info
list[] = _login
list[] = _stub
;...
homepage = index
errorpage = error

[Templates]
root = "./templates"
default = "Basic"
;default = "Monopage"
;delete all not needed chars from output (minimize it)
strip_output = 0

allowed_contents[] = "css"
allowed_contents[] = "js"
allowed_contents[] = "img"
allowed_contents[] = "font"

allowed_extension[] = "css"
allowed_extension[] = "js"
allowed_extension[] = "map"
allowed_extension[] = "jpg"
allowed_extension[] = "png"
allowed_extension[] = "gif"
allowed_extension[] = "ttf"
; template name IS case-sensitive

[Template.Monopage]
allow_button_class[] = "btnSmall"
allow_button_class[] = "btnGreen"
allow_button_class[] = "btnBlue"
allow_button_class[] = "btn"
allow_imageholder_width[] = "fullWidth"
allow_imageholder_width[] = "alignLeft"
allow_imageholder_width[] = "alignRight"

[Plugins]
root = ./plugins
enabled[] = Captcha

[Plugin.Captcha]
static = 0
name = "value"
