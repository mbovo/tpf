# Tiny PHP Framework (TPF) #

***V. 3.0 - red_panda - master***

***by Manuel Bovo***

TPF is really simple to use. It is a framework not a CMS, so it don't write
pages for you! TPF can help you in building a site/blog/cms/whetever you want
giving you some facilities like Database connection, authentication,
captcha generation, ACL, localization ecc...
It is "plugin" capable, so you can write your piece of code to extend any of
core-functionalities.

## Quickstart ##



## HTTP Configuration ##
Keep in mind the *.php files in data/conf/ are world readable and can contains
(for hinstance the datasource.ini) some critical info.
In order to prevent disclosure of critical data you better configure your web server
to avoid access to .php files

#### Entry point and hierarchy ####
The entry point is index.php called from app root /tpf. From this starting point
the main class TPF is called, loading some static class and build up autoloading capabilities.
The App class is the main app called as real box doing the real stuffs

#### Personal Applications ####
You can create your own applications, defining a class which implement the
FrontController interface, editing line 44-45 of TPF.php calling your personal
application using TPF facilities (logging, error handling, locale)


#### Personal Contents ####
You can create your own Controller defining a class which implement the
ManagerController interface, it will loaded automatically calling addresses
like index.php/yourcontroller/yourmethod/yourparams1/yourparams2/.../yourparamsN

#### Personal classes ####
Put your personal classes in class/extra and your global interfaces in class/interfaces
the autoloader will search in these two path for class to automagic-load

#### Templates ####
A semantic template is supported, you can add your *.tpl or *.html files with
html code and special values in order to call variable, functions or class methods

You can add any variable defined in .ini files or created on runtime
<code>
%%varname%%
%%section.varname%%
%%section.arrayname.arraykey%%
</code>

You can call any functions or class method already defined in code:
<code>
{{function(arg1,arg2,...,argN)}}
{{ClassName.method(arg1,arg2,argN)}}
</code>
**Restrictions:**
* You cannot use variables as function/method arguments
* All arguments are used as strings
* function/method called MUST return a valid string or FALSE on error, nothing else.



Have fun!
