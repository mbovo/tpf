<?php

// Class name must implements Template 
// must be the name of the directory underscore Template like that: default_Template
// directory name, class name and name configured in data/conf/ must be the same

class Basic extends Template{

	protected $p_copy = "Designed using Bootstrap";
	protected $p_version = "";

	protected $p_name = __CLASS__;	/*DO NOT EDIT UNLESS YOU KNOW WHAT ARE YOU REALLY DOING!*/



/*
//##############################################################################
	$title		=	array(link, title)
	$links		=   array(
						'text' => 'link',		// for normal links
						'text' => array( 		// for dropdown links
							'text' => 'link',		// inner link
							'text' => '-',			// for headers
							'-'	   => 'anything', 			// for dividers
							)
					)
	$activelink = the n-th link to higlight
	$dark		= navbar bg color
	$form		= the text or array with html of a login form
	$tostring	= print directly (false) or return as string (true)

public function putNavbar($title,$links,
						  $activelink,$dark,$fixed,
						  $static=false,$form=null,$tostring=false);
//##############################################################################

	$level 		a number from 0 to 5 or a string (default, debug, info ....) or a Logger level (see Logger class)
	$dimension	if >0 print also a heading or p tags	
	
public function putLabel($text="Text",$level=0,$dimension=0,$tostring=false);
//##############################################################################
public function putTitle($text,$subtitle=null,$extra=null,$tostring=false)
//##############################################################################
public function putTable($head=array(),$data=array(),$border=false,$striped=false,$condensed=false,$tostring=false);
//##############################################################################
public function putButton($text="Text",$type=0,$size=0,$level=0,$tostring=false);
//##############################################################################
public function putBadge($data=array(),$tostring=false);
//##############################################################################
public function putNav($links=array(),$activelink=0,$tab=false,$pill=false,$tostring=false)
//##############################################################################
public function putJumbo($title="Title",$data="Text",$tostring=false);
//##############################################################################
public funcion putAlert($title='Title',$text='Text',$level=0,$tostring=false);
//##############################################################################
public function putProgressBar($values=10,$min=0,$max=100,$text='Text',$level=0,$tostring=false)
//##############################################################################
putBreadcrumb($list=array(),$activelink=0,$tostring=false)
//##############################################################################
public function putRowMediumColumn($data=array(),$columns=1,$tostring=false)
//##############################################################################
putList($data=array(),$active=0,$linked=false,$generic=false,$tostring=false)
//##############################################################################
putPanel($data=array(),$level=0,$tostring=false )
//##############################################################################
*/
	public function putNavbar($title=array('#','Title'),$links=array(),$activelink=0,$dark=false,$fixed=false,$static=false,$form=null,$tostring=false){
		
		if ($dark)
			$color='navbar-inverse';
		else
			$color='navbar-default';

		$ftop='';		
		if ($fixed)
			$ftop='navbar-fixed-top';
		if ($static)
			$ftop='navbar-static-top';
			
			
		$ret='<!-- Fixed navbar -->
    <div class="navbar '.$color.' '.$ftop.'" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="'.$title[0].'">'.$title[1].'</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">'."\n";
          $n=0;
          foreach ($links as $text => $link){
          		if (is_array($link)){
					$ret.= '            <li class="dropdown">' ."\n";
          			$ret.= '              <a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$text.' <span class="caret"></span></a>'."\n";
					$ret.= '              <ul class="dropdown-menu" role="menu">' ."\n";
					foreach( $link as $t=>$l ){
						if ( $t=='-' )
							$ret.='<li class="divider"></li>'."\n";
						elseif ( $l =='-' )
							$ret.='<li class="dropdown-header">'.$t.'</li>'."\n";
						else
							$ret.=' <li><a href="'.$l.'">'.$t.'</a></li>'."\n";
					}
					$ret.= '              </ul>' ."\n";
					$ret.= '            </li>' ."\n";
          		}else{
		      		if ($n==$activelink)
			      		$ret .= '<li class="active"><a href="'.$link.'">'.$text.'</a></li>' ."\n";
			      	else 
			      		$ret .= '<li><a href="'.$link.'">'.$text.'</a></li>' . "\n";
			      		
		          	$n++;
		        }
          }
          $ret.='          </ul>'."\n";        
          
          if ($form!=null){
          	if (is_array($form))
          		foreach( $form as $entry)
          			$ret .= $entry;
          	else
	          	$ret .= $form;
          }
          
		  $ret.='
       </div><!--/.nav-collapse -->
      </div>
    </div>'."\n";
	
		if ($tostring)
			return $ret;
		else
			$this->out($ret);
	}

	public function putTitle($text,$subtitle=null,$extra=null,$tostring=false){
		$ret='      <div class="page-header">
        <h1>'.$text.'</h1>'."\n";
        
		if ( $subtitle )
			$ret .= '<small>'.$subtitle.'</small>'."\n";
		if ($extra)
			$ret .= $extra;
			
	    $ret .= '</div>'."\n";
   		if ($tostring)
			return $ret;
		else
			$this->out($ret);
	}

	public function putTable($head=array(),$data=array(),$border=false,$striped=false,$condensed=false,$tostring=false){

		$style="";
		if ($border) $style="table-bordered";
		if ($striped) $style="table-striped";
		if ($condensed) $style="table-condensed";
	
		$ret='
		     <table class="table" '.$style.'>
		        <thead>
		          <tr>'."\n";
		foreach ($head as $hline){
			$ret.='                <th>'.$hline.'</th>'."\n";
		}
		$ret.='          </tr>
		        </thead>
		        <tbody>'."\n";
		foreach ($data as $line){
			$ret.='              <tr>'."\n";	
			if (is_array($line)){
				foreach ($line as $column){
					$ret.='                <td>'.$column.'</td>'."\n";
				}	
			}else
				$ret.='                <td>'.$line.'</td>'."\n";
			$ret.='              </tr>'."\n";
		}
		$ret.='            </tbody>
		      </table>'."\n";
		      
   		if ($tostring)
			return $ret;
		else
			$this->out($ret);

	}

	public function putLabel($text="Text",$level=0,$dimension=0,$tostring=false){
		$ret="";
		if ( $dimension > 6 )
			$hat='p';
		else
			$hat='h$dimension';
	
		$label='label ';
		switch ($level){
			case 0:
			case 'default':
			case 'debug':
				$label.='label-default';
				break;
			case 1:
			case 'primary':
				$label.='label-primary';
				break;
			case 2:
			case 'success':
			case 'ok':
				$label.='label-success';
				break;
			case 3:
			case 'info':
			case 'notice':
				$label.='label-info';
				break;
			case 4:
			case 'warning':
					$label.='label-warning';
				break;
			case 5:
			case 'danger':
			case 'critical':
			case 'error':
			case 'alert':
				$label.='label-danger';
				break;
			default:
				$label.='label-default';
		}
	
		if ($dimension!=0)
			$ret .= '<'.$hat.'>'."\n";
		$ret .= '<span class="'.$label.'">'.$text.'</span>'."\n";
		if ($dimension!=0)
			$ret .= '</'.$hat.'>'."\n";
	
   		if ($tostring)
			return $ret;
		else
			$this->out($ret);
	}

	public function putButton($text="Text",$type=0,$size=0,$level=0,$tostring=false){
		if ($type==1)
			$type='submit';
		elseif ($type==2)
			$tpye='cancel';
		else
			$type='button';

		$class = 'btn';
		switch ($size){
			case 0:
				break;
			case 1:
			case 'small':
			case 's':
				$class .= ' btn-xs';
				break;
			case 2:
			case 'medium':
			case 'm':
				$class .=' btn-sm';
				break;
			case 3:
			case 'large':
			case 'l':
				$class .=' btn-lg';
				break;
			default:
				break;
		}

		switch ($level){
			case 0:
			case 'default':
			case 'debug':
				$class.=' btn-default';
				break;
			case 1:
			case 'primary':
				$class.=' btn-primary';
				break;
			case 2:
			case 'success':
			case 'ok':
				$class.=' btn-success';
				break;
			case 3:
			case 'info':
			case 'notice':
				$class.=' btn-info';
				break;
			case 4:
			case 'warning':
				$class.=' btn-warning';
				break;
			case 5:
			case 'danger':
			case 'critical':
			case 'error':
			case 'alert':
				$class.=' btn-danger';
				break;
			case 6:
			case 'link':
				$class.=' btn-link';
				break;		
			default:
				$class.=' btn-primary';
		}
	
		$ret = '<button type="'.$type.'" class="'.$class.'">'.$text.'</button>'."\n";

   		if ($tostring)
			return $ret;
		else
			$this->out($ret);
	}

	public function putBadge($data=array(),$tostring=false){
		$link="#";$text="Text";$count=0;
	
		if (isset($data[0])) $text=$data[0];
		if (isset($data[1])) $link=$data[1];
		if (isset($data[2])) $count=$data[2];
	
		$ret  = '<a href="'.$link.'">'.$text;
		if ($count>0)
			$ret .= ' <span class="badge">'.$count.'</span>'."\n";
		$ret .= '</a>'."\n";
	
		if ($tostring)
			return $ret;
		else
			$this->out($ret);

	}

	public function putJumbo($title="Title",$data="Text",$tostring=false){
		$ret = '      <div class="jumbotron">'."\n";
		$ret .= '<h1>'.$title.'</h1>'."\n";
		if (is_array($data))
			foreach ($data as $line)
				$ret .= $line;
		else
			$ret .= '<p>'.$data.'</p>'."\n";
		$ret .= '</div>'."\n";
	
		if ($tostring)
			return $ret;
		else
			$this->out($ret);
	}

	public function putNav($links=array(),$activelink=0,$tab=false,$pill=false,$tostring=false){
		$class='nav';
		if ($pill) $class.=' nav-pills';
		if ($tab) $class.=' nav-tabs" role="tablist';

		$ret = '<ul class="'.$class.'">'."\n";

		$n=0;
		foreach($links as $text => $link){
			if ($n==$activelink)
				$ret .= '<li class="active"><a href="'.$link.'">'.$text.'</a></li>'."\n";
			else
				$ret .= '<li><a href="'.$link.'">'.$text.'</a></li>'."\n";
			$n++;
		}

		$ret .=' </ul>'."\n";
		
		if ($tostring)
			return $ret;
		else
			$this->out($ret);
	}

	public function putAlert($title='Title',$text='Text',$level=0,$dismiss=false,$tostring=false){
		$class="alert";
		switch ($level){
			case 0:
			case 'success':
			case 'ok':
				$class .= ' alert-success';
				break;
			case 1:
			case 'info':
			case 'i':
				$class .= ' alert-info';
				break;
			case 2:
			case 'warning':
			case 'w':
				$class .= ' alert-warning';
				break;
			case 3:
			case 'danger':
			case 'error':
			case 'ko':
				$class .= ' alert-danger';
				break;
			default:
				$class .= ' alert-success';
				break;
		}
		
		$ret = '<div class="'.$class.'" role="alert">'."\n";
		if ($dismiss)
			$ret .= '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'."\n";
		$ret .= '<strong>'.$title.'</strong>'.$text;
		$ret .= '</div>'."\n";
		
		if ($tostring)
			return $ret;
		else
			$this->out($ret);
	}

	public function putBreadcrumb($list=array(),$activelink=0,$tostring=false){
		$ret ='<ol class="breadcrumb">'."\n";
		
		$n=0;
		foreach ($link as $text => $entry)
			if ($n==$activelink)
				$ret .= '<li class="active"><a href="'.$entry.'">'.$text.'</a></li>'."\n";
			else
				$ret .= '<li><a href="'.$entry.'">'.$text.'</a></li>'."\n";
		
		$ret .= '</ol>'."\n";

		if ($tostring)
			return $ret;
		else
			$this->out($ret);		
	}

	public function putProgressBar($val=10,$min=0,$max=100,$text='Text',$level=0,$tostring=false){
		$class="progress-bar";
		switch ($level){
			case 0:
				break;
			case 1:
			case 'success':
			case 'ok':
				$class .= ' progress-bar-success';
				break;
			case 2:
			case 'info':
			case 'i':
				$class .= ' progress-bar-info';
				break;
			case 3:
			case 'warning':
			case 'w':
				$class .= ' progress-bar-warning';
				break;
			case 4:
			case 'danger':
			case 'error':
			case 'ko':
				$class .= ' progress-bar-danger';
				break;
			case 5:
			case 'striped':
			case 'stripe':
				$class .= ' progress-bar-striped';
				break;				
			default:
				break;
		}
		
		$ret = '<div class="progress">'."\n";
		
		$perc = $val * 100 / $max;	
		$ret .= '<div class="'.$class.'" role="progressbar" aria-valuenow="'.$val.'" aria-valuemin="'.$min.'" aria-valuemax="'.$max.'" style="width: '.$perc.'%;"><span class="sr-only">'.$perc.'% '.$text.'</span></div>'."\n";		
		$ret .= '</div>'."\n";
		if ($tostring)
			return $ret;
		else
			$this->out($ret);
	}

	public function putRowMediumColumn($data=array(),$columns=1,$tostring=false){
	
		$ret ='<div class="row">'."\n";
		$size = ($columns > 0) ? (12/$columns) : (12);
		foreach( $data as $col){
			$ret .= '      <div class="col-sm-'.$size.'">'."\n";
			if (is_array($col))
				foreach($col as $line)
					$ret .= $line;
			else
				$ret .= $col;
			$ret .= '     </div>'."\n";
		}
		$ret .= '  </div>'."\n";
		if ($tostring)
			return $ret;
		else
			$this->out($ret);
	}

	public function putList($data=array(),$active=0,$linked=false,$generic=false,$tostring=false){
	
		$ret = '';	
		$std = (!$linked && !$generic);
		
	
		if ($std){
			$ret ='<ul class="list-group">'."\n";
			foreach ($data as $entry){
				if (is_array($entry)) continue;
				$ret .= '<li class="list-group-item">'.$entry.'</li>'."\n";
			}
			$ret .='</ul>'."\n";
		}

		if 	($linked){
			$ret .='<div class="list-group">'."\n";
			$n=0;
			foreach($data as $text=>$link){
				if ($n==$active)
					$ret .= '<a href="'.$link.'" class="list-group-item active">'.$text.'</a>'."\n";
				else
					$ret .= '<a href="'.$link.'" class="list-group-item">'.$text.'</a>'."\n";
				$n++;
			}
            $ret .= '</div>'."\n";
			$generic=false;	//saveme
		}

		if ($generic){
			$ret .='<div class="list-group">'."\n";
			$n=0;
			foreach($data as $row){
				if ( is_array($row)){
					$link='';$title='';$text='';
					if (isset($row[0])) $link=$row[0];
					if (isset($row[1])) $title=$row[1];
					if (isset($row[2])) $text=$row[2];
						if ($n==$active){
							$ret .= '<a href="'.$link.'" class="list-group-item active">'."\n";
							$ret .= '<h4 class="list-group-item-heading">'.$title.'</h4>'."\n";
							$ret .= '<p class="list-group-item-text">'.$text.'</p>'."\n";
          					$ret .= '</a>'."\n";
						}else{
							$ret .= '<a href="'.$link.'" class="list-group-item">'."\n";
							$ret .= '<h4 class="list-group-item-heading">'.$title.'</h4>'."\n";
							$ret .= '<p class="list-group-item-text">'.$text.'</p>'."\n";
          					$ret .= '</a>'."\n";
						}
						$n++;
				}else{
					if ($n==$active)
						$ret .= '<a href="'.$elink.'" class="list-group-item active">'.$row.'</a>'."\n";
					else
						$ret .= '<a href="'.$elink.'" class="list-group-item">'.$row.'</a>'."\n";
					$n++;				
				}
			}
            $ret .= '</div>'."\n";		
		}

		if ($tostring)
			return $ret;
		else
			$this->out($ret);
	}

	public function putPanel($data=array(),$level=0,$tostring=false ){       
		$ret = '';
		$class="panel";
		switch ($level){
			case 0:
				$class .= ' panel-default';
				break;
			case 1:
			case 'primary':
				$class .= ' panel-primary';
				break;
			case 2:
			case 'success':
			case 'ok':
				$class .= ' panel-success';
				break;
			case 3:
			case 'info':
			case 'i':
				$class .= ' panel-info';
				break;
			case 4:
			case 'warning':
			case 'w':
				$class .= ' panel-warning';
				break;
			case 5:
			case 'danger':
			case 'error':
			case 'e':
				$class .= ' panel-danger';
				break;
			default:
				$class .= ' panel-default';
				break;
		}
		$ret .= '<div class="'.$class.'">'."\n";

		$ret .= '<div class="panel-heading">'."\n";
			if (isset($data['title']))
				$ret .= '<h3 class="panel-title">'.$data['title'].'</h3>'."\n";
		   	if (isset($data['header']))
				$ret .= $data['header'];
        $ret .= '</div>';
        
        if (isset($data['body']))
            $ret .= '<div class="panel-body">'.$data['body'].'</div>'."\n";
		if (isset($data['footer']))
			$ret .= '<div class="panel-footer">'.$data['footer'].'</div>'."\n";
      
        $ret .= '</div>'."\n";

		if ($tostring)
			return $ret;
		else
			$this->out($ret);
	}

}

?>

