<?php

// Class name must implements Template 
// must be the name of the directory underscore Template like that: default_Template
// directory name, class name and name configured in data/conf/ must be the same

class Monopage extends Template{

	protected $p_copy = "Created by <a href=\"http://webcodebuilder.com/\">WebCodeBuilder.com</a>";
	protected $p_version = "";

	protected $p_name = __CLASS__;	/*DO NOT EDIT UNLESS YOU KNOW WHAT ARE YOU REALLY DOING!*/


	// $content = array( "Title1" => "link1", "Title2" => "link2" );
	public function outMenu(array $content,$active=0,$returnstring=false,$logoimg=null){
		
		$ret='		<header id="header">'."\n";
		$ret.='			<div class="center">'."\n";
		$ret.='				<nav id="mainNav">'."\n";
		$ret.='					<ul>'."\n";
		$n=0;
		foreach ( $content as $text=>$link ){
			if ($n++ == $active)
				$ret.='<li class="active"><a href="'.$link.'"><span>'.$text.'</span></a></li>'."\n";
			else
				$ret.='<li><a href="'.$link.'"><span>'.$text.'</span></a></li>'."\n";
		}
		$ret.='					</ul>'."\n";
		if ( isset($logoimg) )
			$ret.='				<img style="position:absolute; top:10px; left:10px; align:left" src="'.$logoimg.'" alt="logo">'."\n";
		$ret.='				</nav>'."\n";
		$ret.='			</div>'."\n";
		$ret.='		</header>'."\n";
		$ret.='		<div id="content">'."\n";
		
		if ( $returnstring )
			return $ret;
		else
			$this->out($ret);
	}

/*
	$content = array(
		"title" => "The Title",
		"subtitle" => "Subtitle text",
		"text" => array(
			0	=>	array() or "",			// text of the first column  
			1	=>	array() or "",			// text of the second column (optional)
			2	=>	array() or "",			// text of the thrind column (optional)
		)
	);
	
	$id 	// used in the field id for internal links
	$grey	// true/false if background change color 
*/
	public function outSection(array $content, $id=null, $grey=false, $box=false){
		if ( $id )
			$firstrow='			<section id="'.$id.'" ';
		else
			$firstrow='			<section '; 
	
		if ( $grey )
			$firstrow .= ' class="row grey">';
		else
			$firstrow .= ' class="row">';
	
		$this->out($firstrow);
		
		$this->out('				<div class="center">');
		if ( isset($content['title']) )
			$this->out('					<h1>'.$content['title'].'</h1>');
		if ( isset($content['subtitle']) )
		$this->out('					<strong class="subHeading">'.$content['subtitle'].'</strong>');	
		
		$col_num = count($content['text']);
		if ( $col_num > 1){
			$this->out('						<div class="columns">');
			if ( $col_num > 2) $col_class = "oneThird";
			else $col_class = "half";
		}
		
		foreach( $content['text'] as $column){
			if ( $col_num > 1 && !$box )	$this->out('							<div class="'.$col_class.'">');
			
			if ( !is_array( $column) )
				$this->out( '								' . $column );
			else
				foreach ( $column as $tline )
					$this->out( '								' . $tline );
		
			if ( $col_num > 1 && !$box )	$this->out('							</div>');
		}
		
		if ( $col_num > 1) $this->out('						</div>');
		$this->out('				</div>');
		$this->out('			</section>');
	}

	public function outImageHolder($link,$width,$returnstring=false){
		$allowed_width=$this->config['allow_imageholder_width'];
		if ( !in_array($width,$allowed_width) ) return "";
		if ( $returnstring )
			return '<div class="imgHolder '.$width.'"><img src="'.$link.'" alt="image" /></div>';
		else
			$this->out('<div class="imgHolder '.$width.'"><img src="'.$link.'" alt="image" /></div>');
	}

	public function outGallery(array $links,$returnstring=false){
		$ret="";
		$ret .= '					<div class="gallery">' . "\n";
		$ret .= '							<ul class="slides">'. "\n";
		foreach ($links as $link)
			$ret .= '								<li><img src="'.$link.'" alt="image" /></li>'. "\n";
		$ret .= '							</ul>'. "\n";
		$ret .= '					</div>'. "\n";

		if ( $returnstring )
			return $ret;
		else
			$this->out($ret);
	}
	
	# buttons = array ( array("www.link.it","class", "Button Title"), ...  )
	public function outButtons(array $buttons, array $separators=null, $returnstring=false){
		$allowed_classes = $this->config['allow_button_class'];
		
		$ret="";
		$ret .= '					<div class="buttons">' ."\n";
		$i=0;
		foreach ($buttons as $button){
			$btlink = $button[0];
			$bttext = $button[2];
			$classes = explode(' ', $button[1]) ;
			$btclass = "";
			if ( $i>0 )
				$ret .= '						<span>&nbsp;</span>'."\n";
			foreach ( $classes as $class )
				if ( in_array($class,$allowed_classes) )
					$btclass .= $class . " ";
				if ( isset($btlink))
					$ret .= '						<a href="'.$btlink.'" class="'.$btclass.'"><span>'.$bttext.'</span></a>'."\n";
				else
					$ret .= '						<a class="'.$btclass.'"><span>'.$bttext.'</span></a>'."\n";
			if ( isset($separators[$i]) )
				$ret .= '						<span><em>'.$separators[$i].'</em></span>'."\n";
			$i++;
		}
		$ret .= '					</div>'."\n";	
		
		if ( $returnstring )
			return $ret;
		else
			$this->out($ret);
	}

	/*
		$content = array(
			'title' => "The Title",
			'link' => "www.link.to/full/article",
			'image' => null or "" with the weblink  (the result of $this->getPath() ),
			'more' => "Read More button text",
			'text' => array() or "" with the small text of the news,
		);
	*/
	public function outNewsBox(array $content,$colclass="oneThird",$returnstring=false){
		$ret = "";
		$ret .= '<article class="news '.$colclass.'">' . "\n";
		$ret .= '							<div id="'.$content['title'].'">'. "\n";
		if ( isset($content['link']) )
			$ret .= '								<h3><a href="'.$content['link'].'">'.$content['title'].'</a></h3>'. "\n";
		else
			$ret .= '								<h3><a href="#'.$content['title'].'">'.$content['title'].'</a></h3>'. "\n";
		$ret .= '								<div class="content">'. "\n";
		
		if ( isset($content['image']) && $content['image'] )
			if ( isset($content['link']) )
				$ret .= '									<a href="'.$content['link'].'" class="imgHolder fullWidth"><img src="'.$content['image'].'" alt="" /></a>' . "\n";
			else
				$ret .= '									<a href="#'.$content['title'].'" class="imgHolder fullWidth"><img src="'.$content['image'].'" alt="" /></a>' . "\n";
		
		foreach ( $content['text'] as $line )
			$ret .= '									'. $line ."\n";
			
		$ret .= '								</div>'. "\n";
		if ( isset($content['link']) && isset($content['more']) )
			$ret .= '								<div class="readMore"><a href="'.$content['link'].'" class="btn btnSmall"><span>'.$content['more'].'</span></a></div>';		
		$ret .= '							</div>'. "\n";
		$ret .= '						</article>'. "\n";

		if ( $returnstring )
			return $ret;
		else
			$this->out($ret); 
	}

	/*
		$content = array(			
			// slides block, only one here disable the slideshow
			array (				
				// 	rows per slide
				array(			
					// 		columns per row
					array("text here"),	//			lines of text
					array("text here"),
				),
				array(
					array("text here"),
					array("text here");
				),
			)	
			// and so on 			
			array(),
		);
	*/
	public function outTestimonials(array $content,$id="testimonials",$returnstring=false){
		$ret = '				<div id="'.$id.'" class="gallery">' ."\n";
		$ret .= '					<ul class="slides">' ."\n";
		foreach ( $content as $slide){
			$ret .= '							<li>'."\n";
			foreach ( $slide as $row ){
				$ret .= '								<div class="row">' ."\n";
				foreach ( $row as $column ){
					$ret .= '									<blockquote>'."\n";
					if ( !is_array($column) )
						$ret .= '										<q>'.$column."</q>\n";
					else
						foreach ( $column as $tline)
							$ret .= '										<q>'.$tline."</q>\n";
					$ret .= '									</blockquote>'."\n";
				}
				$ret .= '								</div>' ."\n";
			}
			$ret .= '							</li>'."\n";
		}
		$ret .= '					</ul>' ."\n";
		$ret .= '				</div>' ."\n";
	
		if ( $returnstring )
			return $ret;
		else
			$this->out($ret);
	}

}
?>
